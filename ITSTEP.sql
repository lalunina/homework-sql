-- ������� ���� ������

IF DB_ID('ITStep') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE ITStep SET single_user with rollback immediate
    DROP DATABASE  ITStep 
END
GO

CREATE DATABASE ITStep
GO

USE ITStep
GO


-- ������� �������


CREATE TABLE Day_Of_The_Weeks
(
    ID bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	
)
GO

CREATE TABLE Offices 
(
    Number bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
)
GO

CREATE TABLE Departments--���������� �� ������
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL, -- �������� �������
)
GO

CREATE TABLE Specializations
(
ID bigint NOT NULL PRIMARY KEY,
Name nvarchar(50) NOT NULL, -- �������� �������������
DepartmentFK bigint
FOREIGN KEY (DepartmentFK) REFERENCES Departments(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,
)
GO

CREATE TABLE  Groups
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	
	N_Students bigint NOT NULL,
    Period numeric, --������ �������� � �����
	SpecializationFK bigint

	FOREIGN KEY (SpecializationFK) REFERENCES Specializations(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,

)
GO



CREATE TABLE  Lecturers-- �������������
(
   Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	FirstName nvarchar(50) NOT NULL,
	LastName nvarchar(50) NOT NULL,
	DepartmentFK bigint

FOREIGN KEY (DepartmentFK) REFERENCES Departments(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,
	
)
GO

CREATE TABLE Clases 
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	DepartmentFK bigint,--�������� �������-- �������� �������, �.�. ��� ���������� ���� � ������ ��������������,
	                    --�� ��� ���������� � ��������� ������� ������ ������, � �. ����� ������ ������
		FOREIGN KEY (DepartmentFK) REFERENCES Departments(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,
)
GO


CREATE TABLE Curses 
(
    Id bigint NOT NULL,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	ClasFC bigint,
	LecturerFC bigint,
    SpecializationFK bigint -- ��� ����� �������������

	FOREIGN KEY (SpecializationFK) REFERENCES Specializations(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,

   FOREIGN KEY (LecturerFC) REFERENCES Lecturers(Id)
	   ON DELETE NO ACTION
       ON UPDATE NO ACTION,

	    FOREIGN KEY (ClasFC) REFERENCES Clases(Id)
	   ON DELETE NO ACTION
       ON UPDATE NO ACTION,
	
	PRIMARY KEY (Id)
)
GO



CREATE TABLE Students
(
    Id bigint NOT NULL ,
	Number int,
	FirstName nvarchar(50) NOT NULL,--���
	LastName nvarchar(50) NOT NULL,--���
	Birth date  CHECK(YEAR(Birth) > 1950),
	GroupFK bigint NOT NULL,
	Semestr bigint,
	Summ_Plan_ALL numeric, --����� ����� �� ���� ������ �������� 
	Summ_Fact_ALL numeric, -- ���������� ����������� �� ������� ������, ���������� �� ������� - ��������� �������� � ������� "�������" � �������� ��������
	                       -- � �������� �������� ��������� �������� �������

	PRIMARY KEY (Id),

	FOREIGN KEY (GroupFK) REFERENCES Groups(Id)
	    ON DELETE NO ACTION
       ON UPDATE NO ACTION,
		

)
GO

CREATE TABLE  Jurnals
(
   ID bigint NOT NULL PRIMARY KEY,
   StudentFK bigint,
   CurseFK bigint,
   Grade bigint,


	FOREIGN KEY (StudentFK) REFERENCES Students(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,


	FOREIGN KEY (CurseFK ) REFERENCES Curses(Id )
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,
)
GO

CREATE TABLE Payments--������/������
(
    Id bigint  NOT NULL IDENTITY  PRIMARY KEY,
	Name bigint NOT NULL,
	Date_Pay date,
	StudentFK bigint,
	Summ numeric,
	Semester int,

	FOREIGN KEY (StudentFK) REFERENCES Students(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,
	
)
GO

CREATE TABLE Schedule
(
Id bigint NOT NULL PRIMARY KEY,
CurseFK bigint,
GroupFK bigint,
OfficeFK bigint,
Day_Of_The_WeekFK bigint,
Time time,

FOREIGN KEY (CurseFK) REFERENCES Curses(ID)
	    ON DELETE NO ACTION
       ON UPDATE NO ACTION,

FOREIGN KEY (GroupFK) REFERENCES Groups(ID)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,

FOREIGN KEY (OfficeFK) REFERENCES Offices (Number)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,

FOREIGN KEY (Day_Of_The_WeekFK) REFERENCES Day_Of_The_Weeks(ID)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,
)
GO
--
---- ��������� �������
--
INSERT INTO Offices VALUES (1)
INSERT INTO Offices VALUES (2)
INSERT INTO Offices VALUES (3)
INSERT INTO Offices VALUES (4)
INSERT INTO Offices VALUES (5)
INSERT INTO Offices VALUES (6)
INSERT INTO Offices VALUES (7)
INSERT INTO Offices VALUES (8)


INSERT INTO Day_Of_The_Weeks VALUES (1, '�����������')
INSERT INTO Day_Of_The_Weeks VALUES (2, '�������')
INSERT INTO Day_Of_The_Weeks VALUES (3, '�����')
INSERT INTO Day_Of_The_Weeks VALUES (4, '�������')
INSERT INTO Day_Of_The_Weeks VALUES (5, '�������')
INSERT INTO Day_Of_The_Weeks VALUES (6, '�������')
INSERT INTO Day_Of_The_Weeks VALUES (7, '�����������')

INSERT INTO Departments VALUES (1, '����������������')
INSERT INTO Departments VALUES (2, '������')
INSERT INTO Departments VALUES (3, '��������� �����������������')

INSERT INTO Specializations VALUES (1, '���', 1)
INSERT INTO Specializations VALUES (2, '���������� ��', 1)
INSERT INTO Specializations VALUES (3, '������', 2)
INSERT INTO Specializations VALUES (4, '��������� �����������������', 1)

INSERT INTO Groups VALUES (1, '2018-��', 8, 2.5, 2)
INSERT INTO Groups VALUES (2, '2017-���', 9, 4, 1)
INSERT INTO Groups VALUES (3, '2218-��', 10, 2.5, 2)
INSERT INTO Groups VALUES (4, '2418-���', 11, 3, 3)
INSERT INTO Groups VALUES (5, '2718-���', 7, 3, 4)


INSERT INTO Clases VALUES (1, '�++', 1)
INSERT INTO Clases VALUES (2, '�#',1)
INSERT INTO Clases VALUES (3, 'Autodesk', 2)
INSERT INTO Clases VALUES (4, 'Adobe Illustrat', 2)
INSERT INTO Clases VALUES (5, '������ ������ ���������� � �������� ������',1)
INSERT INTO Clases VALUES (6, '���������� � ����������� ����������� PC ',3)

INSERT INTO Lecturers VALUES (1, '����������', '��������',1)
INSERT INTO Lecturers VALUES (2, '������', '�������',1)
INSERT INTO Lecturers VALUES (3, '�����', '���������',2)
INSERT INTO Lecturers VALUES (4, '������', '������',3)


INSERT INTO Curses VALUES (1,  1, 1, 1)
INSERT INTO Curses VALUES (2,  1, 1, 2)
INSERT INTO Curses VALUES (3,  1, 2, 2)
INSERT INTO Curses VALUES (4,  2, 3, 3)
INSERT INTO Curses VALUES (5,  3, 3, 1)
INSERT INTO Curses VALUES (6,  4, 3, 2)
INSERT INTO Curses VALUES (7,  3, 3, 3)


--��������
INSERT INTO Students VALUES (1,1, '�������', '��������',  CAST(N'2001-06-12' AS date), 1, 4,50000, 20000)
INSERT INTO Students VALUES (2,2, '�����', '�������',  CAST(N'2001-06-12' AS date), 1, 4,50000, 20000)
INSERT INTO Students VALUES (3,3, '������', '���������',  CAST(N'2001-06-12' AS date),  1,4, 50000, 20000)
INSERT INTO Students VALUES (4,4, '�������', '�������', CAST(N'2001-06-12' AS date),  1,4, 50000, 15000)
INSERT INTO Students VALUES (5,5, '�����', '����',  CAST(N'2001-06-12' AS date), 1,4, 50000, 15000)
INSERT INTO Students VALUES (6,6, '����', '�����', CAST(N'2001-06-12' AS date), 1,4, 50000, 20000)
INSERT INTO Students VALUES (7,7, '�����', '��������',  CAST(N'2001-06-12' AS date), 1,4, 50000, 20000)
INSERT INTO Students VALUES (8,8, '����', '���������',  CAST(N'2001-06-12' AS date),1,4,50000, 20000)

INSERT INTO Students VALUES (9,1, '����������', '����',  CAST(N'14-05-2001' AS date), 2,2, 50000, 20000)
INSERT INTO Students VALUES (10,2, '�����', '��������', CAST(N'20-06-2003' AS date), 2,2, 50000, 20000)
INSERT INTO Students VALUES (11,3, '�������', '����������',CAST(N'25-08-1995' AS date), 2,2, 50000, 20000)
INSERT INTO Students VALUES (12,4, '���������', '��������', CAST(N'26-06-1996' AS date), 2,2, 50000, 20000)
INSERT INTO Students VALUES (13,5, '�������', '������', CAST(N'27-07-1997' AS date), 2, 2,50000, 20000)
INSERT INTO Students VALUES (14,6, '������', '�������', CAST(N'28-08-1998' AS date), 2,2, 50000, 20000)
INSERT INTO Students VALUES (15,7, '�������', '���������', CAST(N'29-09-1999' AS date), 2,2, 50000, 20000)
INSERT INTO Students VALUES (16,8, '������', '�����',CAST(N'02-02-2002' AS date) , 2, 2,50000, 20000)
INSERT INTO Students VALUES (17,9, '������', '�����',CAST(N' 02-02-2002' AS date), 2, 2,50000, 20000)


INSERT INTO Students VALUES (18,1, '�������', '������',CAST(N'11-01-2001' AS date) ,  3,2, 50000, 20000)
INSERT INTO Students VALUES (19,2, '���������', '������',CAST(N'16-05-1999' AS date) , 3,2, 50000, 20000)
INSERT INTO Students VALUES (20,4, '�����', '�����������',CAST(N'27-07-1997' AS date) , 3,2, 50000, 20000)
INSERT INTO Students VALUES (21,5, '������', '�������',CAST(N'21-12-1992' AS date) , 3,2, 50000, 20000)
INSERT INTO Students VALUES (22,6, '�����', '�����',CAST(N'27-07-1997' AS date) , 3,2, 50000, 20000)
INSERT INTO Students VALUES (23,7, '�����', '�������',CAST(N'11-01-2000' AS date) , 3,2, 50000, 20000)
INSERT INTO Students VALUES (24,8, '������', '�������',CAST(N'12-09-1989' AS date) , 3,2, 50000, 20000)
INSERT INTO Students VALUES (25,9, '������', '�����',CAST(N'02-02-2002' AS date) , 3,2,50000, 20000)
INSERT INTO Students VALUES (26,10, '������', '�����',CAST(N'02-02-2002' AS date) , 3,2, 50000, 20000)



--������ ������������

INSERT INTO Jurnals VALUES (1, 1, 5, 10)
INSERT INTO Jurnals VALUES (2, 2, 5, 10)
INSERT INTO Jurnals VALUES (3, 3, 5, 10)
INSERT INTO Jurnals VALUES (4, 4, 5, 8)
INSERT INTO Jurnals VALUES (5, 5, 5, 10)
INSERT INTO Jurnals VALUES (6, 6, 5, 7)
INSERT INTO Jurnals VALUES (7, 7, 5, 10)
INSERT INTO Jurnals VALUES (8, 8, 5, 11)

INSERT INTO Jurnals VALUES ( 9 , 1, 6, 9)
INSERT INTO Jurnals VALUES ( 10, 2, 6, 11)
INSERT INTO Jurnals VALUES ( 11, 3, 6, 10)
INSERT INTO Jurnals VALUES ( 12, 4, 6, 9)
INSERT INTO Jurnals VALUES ( 13, 5, 6, 12)
INSERT INTO Jurnals VALUES ( 14, 6, 6, 7)
INSERT INTO Jurnals VALUES ( 15, 7, 6, 9)
INSERT INTO Jurnals VALUES ( 16, 8, 6, 11)

INSERT INTO Jurnals VALUES ( 17 , 1, 2, 9)
INSERT INTO Jurnals VALUES ( 18, 2, 2, 11)
INSERT INTO Jurnals VALUES ( 19, 3, 2, 10)
INSERT INTO Jurnals VALUES ( 20, 4, 2, 9)
INSERT INTO Jurnals VALUES ( 21, 5, 2, 12)
INSERT INTO Jurnals VALUES ( 22, 6, 2, 7)
INSERT INTO Jurnals VALUES ( 23, 7, 2, 9)
INSERT INTO Jurnals VALUES ( 24, 8, 2, 11)


INSERT INTO Jurnals VALUES ( 25 , 1, 1, 9)
INSERT INTO Jurnals VALUES ( 26, 2, 1, 11)
INSERT INTO Jurnals VALUES ( 27, 3, 1, 10)
INSERT INTO Jurnals VALUES ( 28, 4, 1, 9)
INSERT INTO Jurnals VALUES ( 29, 5, 1, 12)
INSERT INTO Jurnals VALUES ( 30, 6, 1, 7)
INSERT INTO Jurnals VALUES ( 31, 7, 1, 9)
INSERT INTO Jurnals VALUES ( 32, 8, 1, 11)

							 
-- ������ ���������������

INSERT INTO Jurnals VALUES (33, 9 , 5, 9)
INSERT INTO Jurnals VALUES (34, 10, 5, 11)
INSERT INTO Jurnals VALUES (35, 11, 5, 10)
INSERT INTO Jurnals VALUES (36, 12, 5, 9)
INSERT INTO Jurnals VALUES (37, 13, 5, 12)
INSERT INTO Jurnals VALUES (38, 14, 5, 7)
INSERT INTO Jurnals VALUES (39, 15, 5, 9)
INSERT INTO Jurnals VALUES (40, 16, 5, 11)
INSERT INTO Jurnals VALUES (41, 17, 5, 10)


INSERT INTO Jurnals VALUES (42, 9 , 6, 9)
INSERT INTO Jurnals VALUES (43, 10, 6, 5)
INSERT INTO Jurnals VALUES (44, 11, 6, 7)
INSERT INTO Jurnals VALUES (45, 12, 6, 9)
INSERT INTO Jurnals VALUES (46, 13, 6, 9)
INSERT INTO Jurnals VALUES (47, 14, 6, 7)
INSERT INTO Jurnals VALUES (48, 15, 6, 9)
INSERT INTO Jurnals VALUES (49, 16, 6, 11)
INSERT INTO Jurnals VALUES (50, 17, 6, 10)

--������� ����������
INSERT INTO Jurnals VALUES (51, 18, 4, 9)
INSERT INTO Jurnals VALUES (52, 19, 4, 5)
INSERT INTO Jurnals VALUES (53, 20, 4, 7)
INSERT INTO Jurnals VALUES (54, 21, 4, 9)
INSERT INTO Jurnals VALUES (55, 22, 4, 9)
INSERT INTO Jurnals VALUES (56, 23, 4, 7)
INSERT INTO Jurnals VALUES (57, 24, 4, 9)
INSERT INTO Jurnals VALUES (58, 25, 4, 11)
INSERT INTO Jurnals VALUES (59, 26, 4, 10)

INSERT INTO Jurnals VALUES (60, 18, 3, 9)
INSERT INTO Jurnals VALUES (61, 19, 3, 5)
INSERT INTO Jurnals VALUES (62, 20, 3, 7)
INSERT INTO Jurnals VALUES (63, 21, 3, 9)
INSERT INTO Jurnals VALUES (64, 22, 3, 9)
INSERT INTO Jurnals VALUES (65, 23, 3, 7)
INSERT INTO Jurnals VALUES (66, 24, 3, 9)
INSERT INTO Jurnals VALUES (67, 25, 3, 11)
INSERT INTO Jurnals VALUES (68, 26, 3, 10)



INSERT INTO Payments VALUES ( 1, CAST(N'02-02-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 2, CAST(N'01-03-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 3, CAST(N'03-04-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 4, CAST(N'01-02-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 5, CAST(N'10-02-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 6, CAST(N'12-02-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 7, CAST(N'08-02-2002' AS date), 1, 10000, 4)
INSERT INTO Payments VALUES ( 8, CAST(N'09-02-2002' AS date), 1, 10000, 4)


--��
INSERT INTO Schedule VALUES (1, 1,1, 1,1,'9:30:00.1237')
INSERT INTO Schedule VALUES (2, 1,1, 1,1,'11:00:00.1237')
--��
INSERT INTO Schedule VALUES (3, 1,1, 1,2,'9:30:00.1237')
INSERT INTO Schedule VALUES (4, 1,1, 1,2,'11:00:00.1237')
--��
INSERT INTO Schedule VALUES (1, 1,1, 1,3,'9:30:00.1237')
INSERT INTO Schedule VALUES (2, 1,1, 1,3,'11:00:00.1237')
--
GO
---- ���������
--
--SELECT A.Name AS Author, B.Name, P.Name AS Presses
--FROM Authors A, Presses P, Books B -- ������ �� ������ ������
--WHERE A.Id = B.AuthorFk AND P.Id = B.PressFk -- ������� ������ �������� �� ������
--
--SELECT��Name
--
--FROM Books
--
--WHERE��
--
--(SELECT ID
--
--FROM Presses
--
--WHERE Name = '�����')� =� �PressFk