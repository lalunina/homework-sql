--1
SELECT name,IZD, Pressrun

FROM Books
WHERE IZD!= 'BHV ����' AND Pressrun >=3000

--2  �������� �����, �� ��� ������� ������� ������ �� ����� ���� c2000 �� 2001 �.
-- ������ ���������� ���� ����� � ������� ������� DATEFROMPARTS().

SELECT name, [Date]
FROM Books
WHERE  [Date] BETWEEN DATEFROMPARTS ( 2000, 02, 17 ) AND DATEFROMPARTS ( 2001, 02, 17 )

--3. �������� �����, � ���� ������� ������� ������ �� ��������.
SELECT name, [Date]
FROM Books
WHERE  [Date] IS NULL

--4.�������� ��� �����-�������, ���� ������� ���� 30 ���.
SELECT name, New, Price
FROM Books
WHERE  New = 1 AND Price<30.0

--5. �������� �����, � ��������� ������� ���� ����� Microsoft, �� ��� ����� Windows.

SELECT name 
FROM Books
WHERE name LIKE '%[M][i][c][r][o][s][o][f][t]%' AND name NOT LIKE '%[[W][i][n][d][o][w][s]%'

--6. �������� �����, � ������� ���� ����� �������� < 10 ������.

SELECT name, Price/Pages   AS PricePages
FROM Books
WHERE Pages !=0 AND Price/Pages< 0.10

--7. �������� �����, � ��������� ������� ������������ ��� ������� ���� �����.

SELECT Name 
FROM Books
WHERE Name LIKE '%[0-9]%'
-- 8. �������� �����, � ��������� ������� ������������ �� ����� ���� ����.
SELECT Name 
FROM Books
WHERE Name LIKE '%[0-9]%[0-9]%[0-9]%'
--9. �������� �����, � ��������� ������� ������������ ����� ���� ����.  
SELECT Name 
FROM Books
WHERE Name LIKE '%[0-9]%[0-9]%[0-9]%[0-9]%[0-9]%'  
--WHERE Name LIKE '%[0-9]%{5}' 
--10.    ������� �����, � ���� ������� ������������ ����� 6 ��� 7.
DELETE 
FROM Books
WHERE Code LIKE '%[6-7]%'
--11.    ���������� ������� ���� ��� ��� ����, � ������� ���� ������� �����������.
 --T������ ���� ����� �������� � ������� ������� GETDATE().


UPDATE Books
SET [Date] = GETDATE()
WHERE YEAR ([Date]) IS NULL


SELECT Name, [Date]
FROM Books
ORDER BY YEAR ([Date]) DESC