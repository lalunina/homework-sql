--1. ������� ���������� � ����� � ���������� ����������� �������.
SELECT MAX(Pages) AS MaxPages
FROM Books;
GO


--2. ������� ���������� � ����� �� ���������������� � ���������� ����������� �������.

SELECT A.Name,  MAX(B.Pages) AS Pagis
FROM Themes A, Books B
WHERE A.Name='����������������'
GROUP BY A.Name

--3. ������� ���������� ��������� ���������� �� ������ ������ ���������.

SELECT G.Name, COUNT(S.StudentFk) AS visit 
FROM Students A, Groups G, StudentCards S
WHERE A.Id = S.StudentFk AND A.GroupFk = G.Id
GROUP BY G.Name
--HAVING COUNT(G.Name) >= 3;


--4. ������� ���������� ����, ������ � ���������� �������������� �� ���������
--   ����������������� � ����� �������, � ����� ������� � ���� ������.

SELECT   COUNT(B.Name) AS ����������_����, SUM (B.Pages) AS Pages
FROM    Books B
WHERE B.ID IN
(SELECT SC.BookFk FROM StudentCards SC Where SC.StudentFk IN
 (SELECT A.Id FROM Students A Where A.GroupFk IN
   (SELECT G.Id  From  Groups G Where G.FacultyFk IN
	 (SELECT F.Id From Faculties F  WHERE F.Name =  '����������������' ))))
	 AND
	 B.ThemeFk IN 
	 (SELECT T.Id FROM Themes T WHERE T.Name = '����������������' OR T.Name = '���� ������');


-- �������� �������������

CREATE VIEW ST_F_PROGR 
AS
SELECT A.Id as ID, A.FirstName, G.ID as G_id, F.Name AS Faculty
FROM  (Faculties F JOIN Groups G ON G.FacultyFk = F.Id AND F.Name =  '����������������' ) JOIN Students A ON A.GroupFk =G.Id;


SELECT  B.Name, B.Pages, T.Name AS ����, ST.Faculty, ST.FirstName, ST.ID
FROM ( (Books B JOIN Themes T ON B.ThemeFk = T.Id    ) 
                JOIN StudentCards S ON S.BookFk=B.Id) 
				JOIN ST_F_PROGR ST ON S.StudentFk = ST.Id
	WHERE T.Name = '����������������' OR T.Name = '���� ������';

 SELECT T.Name, COUNT(S.BookFk) AS ����������_����, SUM (B.Pages) AS Pages
 FROM ( (Books B JOIN Themes T ON B.ThemeFk = T.Id)
                 JOIN StudentCards S ON S.BookFk=B.Id) 
				 JOIN ST_F_PROGR ST ON S.StudentFk = ST.Id
WHERE T.Name = '����������������' OR T.Name = '���� ������'
GROUP BY T.Name;



--5. ��������, ��� ������� ����� ����� ������� ����� � ���� ���� ������ 1 �����,
--   � �� ������ ������ ����� ���� �� ������ ����������� ���������� ������������ ������ ����������
--   ���������� (������� ������� 0.5 �) �������� ����. ���������� ������� ������� ������
--   ������ ������ �������, � ����� ��������� ���������� ������ ����. �������� ����� ������
--   ����������� � ������� �������, �� ���� ��������� ����� ������ ������ ���� �����.
--   ����������� ������� DATEDIFF � CAST.

-- ����
SELECT S.FirstName AS �������, SC.DateOut AS ����,  SC.DateIn AS ������, L.FirstName AS ������������,(DATEDIFF(week,SC.DateOut, DATEFROMPARTS (YEAR( SC.DateIn),  MONTH(  SC.DateIn)-1,DAY( SC.DateIn))))  AS ������_�_������, CAST ((DATEDIFF(week,SC.DateOut, DATEFROMPARTS (YEAR( SC.DateIn),  MONTH(  SC.DateIn)-1,DAY( SC.DateIn)))) *0.5 AS numeric) AS ����_�_������
FROM (StudentCards SC JOIN Students S ON SC.StudentFk =S.Id) 
       JOIN Libs L ON SC.LibFk = L.Id
	   WHERE L.FirstName = '������' AND DATEDIFF(Month, SC.DateOut, SC.DateIn) > 1   
UNION 
SELECT S.FirstName AS �������, SC.DateOut AS ����,  SC.DateIn AS ������, L.FirstName AS ������������,(DATEDIFF(week,DATEFROMPARTS (YEAR( SC.DateOut),  MONTH( SC.DateOut)+1,DAY( SC.DateOut)), '2002.01.01'))  AS ������_�_������, CAST ((DATEDIFF(week,DATEFROMPARTS (YEAR( SC.DateOut),  MONTH( SC.DateOut)+1,DAY( SC.DateOut)), '2002.01.01')) *0.5 AS numeric) AS ����_�_������
FROM (StudentCards SC JOIN Students S ON SC.StudentFk =S.Id) 
       JOIN Libs L ON SC.LibFk = L.Id
	   WHERE L.FirstName = '������' AND DATEDIFF(Month, SC.DateOut, '2002.01.01') > 1 AND SC.DateIn IS NULL;

--6. ���� ������� ����� ���������� ���� � ���������� �� 100%, �� ���������� ����������
--   ������� ���� (� ���������� ���������) ���� ������ ���������.


-- �������� �������������

CREATE VIEW ST_F 
AS
SELECT A.Id as ID, A.FirstName, G.ID as G_id, F.Name AS Faculty
FROM  (Faculties F JOIN Groups G ON G.FacultyFk = F.Id ) JOIN Students A ON A.GroupFk =G.Id;

 SELECT ST.Faculty, COUNT(SC.BookFk) AS ����������_����, (SELECT Count(B.Name) FROM Books B) AS Book_ALL,
       COUNT(SC.BookFk)*100/(SELECT Count(B.Name) FROM Books B) AS PERSENT
 FROM  (Books B JOIN StudentCards SC ON SC.BookFk=B.Id) 
				 JOIN ST_F ST ON SC.StudentFk = ST.Id

GROUP BY ST.Faculty; 




--������� �������

SELECT F.Name, COUNT(B.Id)*100/(SELECT Count(B.Name) FROM Books B) AS �������, COUNT(B.Id) AS ����������, (SELECT Count(B.Name) FROM Books B) AS �����_����
 FROM(((Books B JOIN StudentCards SC ON SC.BookFk=B.Id) 
				 JOIN Students S ON S.Id= SC.StudentFk)
				  JOIN Groups G ON G.Id= S.GroupFk )
				  JOIN Faculties F ON F.Id= G.FacultyFk
GROUP BY F.Name;

--SELECT F.Name, B.Name, S.FirstName, G.Name
-- FROM(((Books B JOIN StudentCards SC ON SC.BookFk=B.Id) 
--				 JOIN Students S ON S.Id= SC.StudentFk)
--				  JOIN Groups G ON G.Id= S.GroupFk )
--				  JOIN Faculties F ON F.Id= G.FacultyFk
--ORDER BY G.Name;
--------------------------------------------------

--7. ������� ������ ����������� ������(��) ����� ���������.

SELECT top (1)  with TIES A.FirstName,A.LastName,  COUNT (B.Name) as N_books--, B.Name, B.AuthorFk,SC.BookFk
 FROM(Authors A JOIN Books B ON B.AuthorFk = A.ID)
                 JOIN StudentCards SC ON SC.BookFk=B.Id 
				 			  
GROUP BY A.FirstName, A.LastName order BY N_books desc;



--8. ������� ������ ����������� ������(��) ����� �������������� � ���������� ����
--   ����� ������, ������ � ����������.


SELECT top (1)  with TIES T.Name, COUNT(B.ThemeFk) AS N_books

 FROM Themes T JOIN Books B ON T.Id=B.ThemeFk
 WHERE B.Id IN (SELECT TC.BookFk FROM TeacherCards TC)
  OR B.Id IN( SELECT SC.BookFk FROM StudentCards SC)

GROUP BY T.Name order BY N_books desc ;


--10. ����������� ����� ����� ��� ���������, ������� �� ������� ����� ����� ����,
--   �.�. � ������� ��������� ��������� (StudentCards) ��������� ���� ����� ��������
--   (DateIn) ������� �����.


UPDATE StudentCards 
SET DateIn = GETDATE ( ) 
WHERE  DateIn IS NULL OR DATEDIFF(YEAR,DateOut, DATEFROMPARTS (YEAR(DateIn),  MONTH(DateIn)-1,DAY(DateIn))) >=1

--11.	������� �� ������� ��������� ��������� ���������, ������� ��� ������� �����.
--DELETE ;

DELETE
FROM Students 
WHERE ID IN ( SELECT StudentFk FROM StudentCards WHERE DateIn < GETDATE() );
