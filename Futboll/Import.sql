USE Football

-- �������� ������� ����� ��
CREATE TABLE Players5
(
    Id INT PRIMARY KEY IDENTITY,
    Position int,
    Name NVARCHAR(30)
)
GO

-- ��������� ������� ��� �������. ���� ����� ������������� ����� � ������������� ��������� �����.
CREATE TABLE PlayersTemp1
(
    Position NVARCHAR(30),
    Name NVARCHAR(55)
)
GO

-- ����������� �� ��������� ������� ������ �� ���������� ����� 
BULK INSERT PlayersTemp1
FROM 'C:\Downloads\test.txt'
WITH (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n')
GO

-- �������� ������ �� ��������� ������� � ��������. ��� ���� ��������� ����
-- � �������� ������� ����� �������������� ������������� (�� �� �������� ��� IDENTITY)
INSERT INTO Players5 (Position, Name)
SELECT TRY_CONVERT(int, Position) , Name
FROM PlayersTemp1
GO

-- ������� ����� �� ������ ��������� �������
DROP TABLE PlayersTemp1
GO

-- ���������
SELECT * FROM Players5
GO
