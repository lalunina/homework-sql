USE Football


CREATE TABLE Comands
(
    ID int NOT NULL PRIMARY KEY,
    Name NVARCHAR(30),
	Triner NVARCHAR(30),
	CountreFK int,
	EquipmentFK int,



)
GO


-- ����������� � ������� ������ ��  ����� csv
BULK INSERT Comands
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\CopiComands.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n', CODEPAGE = 'ACP')
GO

-- �������� ������ �� ��������� ������� � ��������. ��� ���� ��������� ����
-- � �������� ������� ����� �������������� ������������� (�� �� �������� ��� IDENTITY)


-- ���������
--SELECT * FROM Comands
--GO

CREATE TABLE Countries
(
   Name_Countries nvarchar(50),
   Id_Countries bigint IDENTITY NOT NULL PRIMARY KEY,

)
GO


BULK INSERT Countries
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\countries.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n')
GO

--SELECT * FROM Countries
--GO

CREATE TABLE Players
(
    ID int NOT NULL PRIMARY KEY,
	Number int,
    Name NVARCHAR(30),
	ComanFK int,
	PozitionFK int,
)
GO

BULK INSERT Players
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\Players.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n')
GO

SELECT * FROM Players
GO


CREATE TABLE Referees
(
    Id int NOT NULL PRIMARY KEY,
	AssociationFK int,
	CountrieFK int,
	Name varchar(50)
)

BULK INSERT Referees
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\referee.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n',CODEPAGE = 'ACP')
GO

SELECT * FROM Referees
GO

CREATE TABLE Stadiums
(
    Id int NOT NULL PRIMARY KEY,
	NameSity varchar(50),
	NameStadium varchar(50),
	Capacity int

)

BULK INSERT Stadiums
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\Studiums.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n',CODEPAGE = 'ACP')
GO

SELECT * FROM Stadiums
GO


