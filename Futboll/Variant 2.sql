USE Football;

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InsertGroups' AND type = 'P')
    DROP PROCEDURE InsertGroups;
GO

CREATE PROCEDURE InsertGroups 
AS
  DECLARE @i int , @a int, @b int
  SET @i=1
  SET @b=1

 While @b<=8
BEGIN
	 SET @a=1 
	 While @a<=4
BEGIN
     insert into Groups ( NumberInGroup,GroupNumber, ComandFK ) values (@a, @b, @i)
	 SET @i=@i+1
     SET @a=@a+1 
END
    SET @b=@b+1   
END
   Set NoCount OFF

  --SELECT * FROM Groups;
GO

--1

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'UP_Qualifying_Matchs' AND type = 'P')
    DROP PROCEDURE UP_Qualifying_Matchs;
GO

CREATE PROCEDURE UP_Qualifying_Matchs
AS
	UPDATE Qualifying_Matchs SET Goals1 = rand(checksum(newid()))*3;
	UPDATE Qualifying_Matchs SET Goals2 = rand(checksum(newid()))*3;
	UPDATE Qualifying_Matchs SET RefereesFK = 1+ rand(checksum(newid()))*29;  
	UPDATE Qualifying_Matchs SET StadiumFK =1+ rand(checksum(newid()))*12;
	UPDATE Qualifying_Matchs SET Viewers = 3000+rand(checksum(newid()))*(SELECT Stadiums.Capacity - 3000 FROM Stadiums WHERE Stadiums.Id = Qualifying_Matchs.StadiumFK);

	UPDATE Qualifying_Matchs SET Points1 = 3, Points2 = 0 WHERE Goals1 > Goals2;
    UPDATE Qualifying_Matchs SET Points1 = 1, Points2 = 3 WHERE Goals1 = Goals2;
	UPDATE Qualifying_Matchs SET Points1 = 0, Points2 = 0 WHERE Goals1 < Goals2;

SELECT * FROM Qualifying_Matchs;
GO

--2

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InsertOneEighth' AND type = 'P')
    DROP PROCEDURE InsertOneEighth;
GO

CREATE PROCEDURE InsertOneEighth
AS

DECLARE @UP_Resulte TABLE (ID int, Teams int, Groups int, Points int);

INSERT INTO  @UP_Resulte  SELECT ID, Team2IDFK AS Teams, Group_N AS Group_N,  Points2 AS Point
FROM Qualifying_Matchs
UNION
SELECT ID,  Team1IDFK AS Teams, Group_N AS Group_N,  Points1 AS Point
FROM Qualifying_Matchs; 

--SELECT * FROM @UP_Resulte;

DECLARE @Group_Resulte TABLE (ID int IDENTITY, Teams int, Groups int, Points int); --����������� �� �������, � �.�. ����������� ������������ ������ ����������� �����������
INSERT INTO  @Group_Resulte SELECT U.Teams, U.Groups, SUM (U.Points) AS Points 
FROM @UP_Resulte U
GROUP BY   U.Teams, U.Groups; 

--SELECT * FROM @Group_Resulte;

DECLARE @table TABLE
( Points int,  Teams int, Group_N int, ID int IDENTITY )

DECLARE @i int;
SET @i=1;

WHILE @i <9
BEGIN

INSERT INTO @table  SELECT TOP (2) U.Points, U.ID, U.Groups--�������� �������� ����������� ������� � ���� tems, ID - ��� ����� ������� ��������� � 1\8
 FROM @Group_Resulte U
 WHERE  U.Groups =@i
 ORDER BY Points DESC;

 SET @i=@i+1;
  
END
 
-- SELECT * FROM @table;  -- ���������� ����������� �����������
  
 UPDATE �hampionship SET Teams1 = (SELECT t.Teams FROM @table t WHERE ID = Teams1) WHERE PlayOfFK =2;
 UPDATE �hampionship SET Teams2 = (SELECT t.Teams FROM @table t WHERE ID = Teams2) WHERE PlayOfFK =2;

 GO
 --3


IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Insert_PlayOff' AND type = 'P')
    DROP PROCEDURE Insert_PlayOff;
GO

CREATE PROCEDURE Insert_PlayOff (@i int )
AS

 UPDATE �hampionship SET Goals1 = rand(checksum(newid()))*3 WHERE PlayOfFK =@i;
 UPDATE �hampionship SET Goals2 = rand(checksum(newid()))*3 WHERE PlayOfFK =@i;
 UPDATE �hampionship SET RefereesFK = 1+ rand(checksum(newid()))*29 WHERE PlayOfFK =@i;  
 UPDATE �hampionship SET StadiumFK =1+ rand(checksum(newid()))*12 WHERE PlayOfFK =@i;
 UPDATE �hampionship SET Viewers = 3000+rand(checksum(newid()))*(SELECT Stadiums.Capacity - 3000 FROM Stadiums WHERE Stadiums.Id = �hampionship.StadiumFK) WHERE PlayOfFK =@i;

 UPDATE �hampionship SET Points1 = 3, Points2 = 0 WHERE Goals1 > Goals2 AND PlayOfFK =@i;
 UPDATE �hampionship SET Points1 = 1, Penalty1 = 1+rand(checksum(newid()))*2, Penalty2 = rand(checksum(newid()))*1 WHERE Goals1 = Goals2  AND PlayOfFK =@i;
 UPDATE �hampionship SET Points1 = 0, Points2 = 3 WHERE Goals1 < Goals2;

 UPDATE �hampionship SET Points1 = 3, Points2 = 0  WHERE  Goals1 = Goals2 AND Penalty1>Penalty2 AND PlayOfFK =@i;
 UPDATE �hampionship SET Points1 = 0, Points2 = 3 WHERE  Goals1 = Goals2 AND Penalty1<Penalty2 AND PlayOfFK =@i;

 SELECT * FROM �hampionship WHERE PlayOfFK =@i;
 GO


 IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Insert_1_4' AND type = 'P')
    DROP PROCEDURE Insert_1_4;
GO

CREATE PROCEDURE Insert_1_4 
AS

 DECLARE @INSERT1_4 TABLE (ID int IDENTITY, Teams int); --������� ��������� ������� � ��������� ������������
 
 INSERT INTO @INSERT1_4 ( Teams)
 SELECT  Teams1 FROM �hampionship WHERE Points1 = 3 AND PlayOfFK =2
 UNION
 SELECT  Teams2 FROM �hampionship WHERE Points2 = 3 AND PlayOfFK =2;

 --SELECT * FROM @INSERT1_4;


UPDATE �hampionship SET Teams1 = (SELECT I.Teams FROM @INSERT1_4  I WHERE I.ID = Teams1) WHERE PlayOfFK = 3;
UPDATE �hampionship SET Teams2 = (SELECT I.Teams FROM @INSERT1_4 I WHERE I.ID = Teams2) WHERE PlayOfFK = 3;

 --SELECT * FROM �hampionship WHERE PlayOfFK = 3;

 GO


  IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Insert_1_2' AND type = 'P')
    DROP PROCEDURE Insert_1_2;
GO

CREATE PROCEDURE Insert_1_2 
AS

 DECLARE @INSERT1_4 TABLE (ID int IDENTITY, Teams int); --������� ��������� ������� � ��������� ������������
 
 INSERT INTO @INSERT1_4 ( Teams)
 SELECT  Teams1 FROM �hampionship WHERE Points1 = 3 AND PlayOfFK =3
 UNION
 SELECT  Teams2 FROM �hampionship WHERE Points2 = 3 AND PlayOfFK =3;

 --SELECT * FROM @INSERT1_4;


UPDATE �hampionship SET Teams1 = (SELECT I.Teams FROM @INSERT1_4  I WHERE I.ID = Teams1) WHERE PlayOfFK = 4;
UPDATE �hampionship SET Teams2 = (SELECT I.Teams FROM @INSERT1_4 I WHERE I.ID = Teams2) WHERE PlayOfFK = 4;

 --SELECT * FROM �hampionship WHERE PlayOfFK = 4;

 GO



  IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Insert_3' AND type = 'P')
    DROP PROCEDURE Insert_3;
GO

CREATE PROCEDURE Insert_3 
AS

 DECLARE @INSERT TABLE (ID int IDENTITY, Teams int); --������� ��������� ������� � ��������� ������������ � ����������
 INSERT INTO @INSERT ( Teams)
 SELECT  Teams1 FROM �hampionship WHERE Points1 = 0 AND PlayOfFK =4
 UNION
 SELECT  Teams2 FROM �hampionship WHERE Points2 = 0 AND PlayOfFK =4;

 --SELECT * FROM @INSERT;


UPDATE �hampionship SET Teams1 = (SELECT I.Teams FROM @INSERT  I WHERE I.ID = Teams1) WHERE PlayOfFK = 5;
UPDATE �hampionship SET Teams2 = (SELECT I.Teams FROM @INSERT I WHERE I.ID = Teams2) WHERE PlayOfFK = 5;

--SELECT * FROM �hampionship WHERE PlayOfFK = 5;
EXEC Insert_PlayOff 5;
 GO

 
  IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Final' AND type = 'P')
    DROP PROCEDURE Final;
GO

CREATE PROCEDURE Final 
AS

 DECLARE @INSERT TABLE (ID int IDENTITY, Teams int); --������� ��������� ������� � ��������� ������������
 
 INSERT INTO @INSERT ( Teams)
 SELECT  Teams1 FROM �hampionship WHERE Points1 = 3 AND PlayOfFK =4
 UNION
 SELECT  Teams2 FROM �hampionship WHERE Points2 = 3 AND PlayOfFK =4;

 --SELECT * FROM @INSERT;


UPDATE �hampionship SET Teams1 = (SELECT I.Teams FROM @INSERT  I WHERE I.ID = Teams1) WHERE PlayOfFK = 6;
UPDATE �hampionship SET Teams2 = (SELECT I.Teams FROM @INSERT I WHERE I.ID = Teams2) WHERE PlayOfFK = 6;

 --SELECT * FROM �hampionship WHERE PlayOfFK = 6;
 EXEC Insert_PlayOff 6;
 GO


 IF EXISTS (SELECT * FROM sys.objects WHERE name = 'PlayChampoinship' AND type = 'P')
    DROP PROCEDURE PlayChampoinship;
GO


CREATE PROCEDURE PlayChampoinship
AS 
--1
EXEC InsertGroups

--2
EXEC UP_Qualifying_Matchs

 --3
EXEC InsertOneEighth

--4
EXEC Insert_PlayOff 2;

--5
EXEC Insert_1_4;

--6
EXEC Insert_PlayOff 3;

--7
EXEC Insert_1_2;

--8
EXEC Insert_PlayOff 4;
--9

EXEC Insert_3;
--10
EXEC Final;

--�������� ��������� �������:

--10. ������ ������, �������� �������� �����

SELECT *, (SELECT P.Name FROM Play_Off P WHERE PlayOfFK = P.ID) AS ����,(SELECT C.Name FROM Comands C WHERE Teams1 = C.ID) AS NameComand1, (SELECT C.Name FROM Comands C WHERE Teams2 = C.ID) AS NameComand2 FROM �hampionship;


 SELECT  CH.Teams1 AS Champion,  C.Name FROM �hampionship CH JOIN Comands  C ON  CH.Teams1 = C.ID WHERE Points1 = 3 AND PlayOfFK =6
 UNION
 SELECT  CH.Teams2 AS Champion,  C.Name FROM �hampionship CH JOIN Comands  C ON  CH.Teams2 = C.ID WHERE Points2 = 3 AND PlayOfFK =6;

 SELECT  CH.Teams1 AS Second_place,  C.Name FROM �hampionship CH JOIN Comands  C ON  CH.Teams1 = C.ID WHERE Points1 = 0 AND PlayOfFK =6
 UNION
 SELECT  CH.Teams2 AS Second_place,  C.Name FROM �hampionship CH JOIN Comands  C ON  CH.Teams2 = C.ID WHERE Points2 = 0 AND PlayOfFK =6;

 SELECT  CH.Teams1 AS Third_place,  C.Name FROM �hampionship CH JOIN Comands  C ON  CH.Teams1 = C.ID  WHERE Points1 = 3 AND PlayOfFK =5
 UNION
 SELECT  CH.Teams2 AS Third_place,  C.Name FROM �hampionship CH JOIN Comands  C ON  CH.Teams2 = C.ID WHERE Points2 = 3 AND PlayOfFK =5;

 GO


 --��� ������� (1-9)

 --USE Football;
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'SELECTS' AND type = 'P')
    DROP PROCEDURE SELECTS;
GO


CREATE PROCEDURE SELECTS
AS 

  --1. ����� ���������� �����, ������� �� ���������� ����������
 DECLARE @OrderDetails TABLE  ( IDMatch int,  Teams int, GoolMy int, GoolMe int, Point int, NameComand nvarchar(25), ID int IDENTITY);
 INSERT INTO @OrderDetails
 SELECT ID, Teams1 AS Teams, 
     (CASE   
      WHEN Penalty1 IS NULL THEN Goals1 
	  ELSE  Penalty1+Goals1 END ) AS GoolMy,    
	  (CASE   
      WHEN Penalty2 IS NULL  THEN Goals2
	  ELSE  Penalty2+Goals2  END ) AS GoolMe, 
	  Points1 AS Point, (SELECT C.Name FROM Comands C WHERE Teams1 = C.ID) AS NameComand 
 FROM �hampionship						  	    			  
 UNION										   						    
 SELECT ID, Teams2 AS Teams, 
	  (CASE   
      WHEN Penalty2 IS NULL THEN Goals2
	  ELSE  Penalty2+Goals2 END ) AS GoolMy,
	  (CASE   
      WHEN Penalty1 IS NULL THEN Goals1 
	  ELSE  Penalty1+Goals1 END ) AS GoolMe, 
	  Points2 AS Point, (SELECT C.Name FROM Comands C WHERE Teams2 = C.ID) AS NameComand 
 FROM �hampionship;					  	    			  
								   
  --SELECT * FROM @OrderDetails;

  SELECT  SUM (GoolMy) AS �����_����������_�����
  FROM @OrderDetails;

  --2. ������� ���������� ����� � ������ �����
  SELECT ID, AVG (GoolMy) AS �������_����������_�����
  FROM @OrderDetails
  GROUP BY ID;

--3. ���������� � ���������� ���������� �����, ������� ��������� (2 �������) \\ ���������
  --SELECT  SUM (GoolMy) AS MIN_�����, NameComand
  --FROM @OrderDetails
  --GROUP BY NameComand ORDER BY MIN_�����; 

SELECT TOP 2 SUM (GoolMy) AS MAX_�����, NameComand
  FROM @OrderDetails
  GROUP BY NameComand 
  ORDER BY MAX_����� DESC;

--4. ���������� � ���������� ���������� �����, ����������� ��������� (2 �������)
 SELECT TOP 2 SUM (GoolMy) AS MIN_�����, NameComand
  FROM @OrderDetails
  GROUP BY NameComand ORDER BY MIN_�����;

--5. ��������� ������������ ���� ������ \\ ��������� �����
SELECT SUM (Viewers) AS ���������_������������ FROM �hampionship;

--6. ������� ������������ ������ ����� \\ 
SELECT AVG (Viewers) AS �������_������������ FROM �hampionship;

--7. ���������� � ���������� ����� ����� (2 ������������ �������) \\??
SELECT TOP 2 SUM (Point) AS MAX_�����, NameComand
  FROM @OrderDetails
  GROUP BY NameComand ORDER BY MAX_����� DESC ;
--8. ���������� � ���������� ���������� ��������� (2 �������)

SELECT TOP 2 SUM (Point) AS MAX_�����, NameComand
  FROM @OrderDetails
  GROUP BY NameComand ORDER BY MAX_�����  ;
--9. ������ ���������� (������, �������� ���������� ���������� �����)
--�������� ������

DECLARE  @Main_cast TABLE( IDPlayers int, IDComand int, ID int IDENTITY);
DECLARE  @i int;
SET @i=1;
WHILE (@i<33)
BEGIN
 INSERT INTO @Main_cast SELECT TOP (11) ID, ComanFK FROM Players  WHERE ComanFK = @i; -- �� ����������� �������� ���� �� ����������.
 SET @i=@i+1;
END

 --SELECT * FROM @Main_cast;

DECLARE @BombardiersStatostic TABLE  (IDMatch int, Teams int, Minuts int, IDPlayers int, ID int IDENTITY );

 DECLARE @id  int, @gools int;
 SET @id = 1;
 SET @gools = 1;

 While @id<=(SELECT MAX(ID) FROM @OrderDetails)-- 32 pfgbcb
BEGIN
	 SET @gools=1 
	 While @gools<=(SELECT GoolMy FROM  @OrderDetails WHERE ID = @id)
BEGIN
     INSERT INTO @BombardiersStatostic SELECT  O.IDMatch AS IDMatch, O.Teams AS Teams, 1+ rand(checksum(newid()))*90, 
	((SELECT MIN (M.ID) FROM @Main_cast M JOIN @OrderDetails O ON M.IDComand = O.Teams WHERE O.ID=@id)+rand(checksum(newid()))*(SELECT MAX (M.ID) FROM @Main_cast M JOIN @OrderDetails O ON M.IDComand = O.Teams WHERE O.ID=@id))
	   FROM @OrderDetails O WHERE O.ID=@id; 
	  
	 SET @gools = @gools +1;
END
    SET @id =@id +1 ;
END
   Set NoCount OFF;

--SELECT *  FROM @BombardiersStatostic;

DECLARE @GoolsUnion TABLE  (IDMatch int, TeamsID int , TeamsNAME nvarchar(25), IDPlayers int, PlayersName nvarchar(25), ID int IDENTITY );

INSERT INTO @GoolsUnion SELECT B.IDMatch, B.Teams, C.Name,  M.IDPlayers, P.Name   FROM ((@BombardiersStatostic B JOIN @Main_cast M ON B.IDPlayers = M.ID) JOIN Players P ON M.IDPlayers = P.ID) JOIN Comands C ON C.ID =P.ComanFK;


SELECT TOP (5) PlayersName, COUNT(PlayersName) AS COL FROM @GoolsUnion
GROUP BY PlayersName
ORDER BY COL DESC;

 GO



 --�����������

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Replay' AND type = 'P')
    DROP PROCEDURE Replay;
GO


CREATE PROCEDURE Replay
AS 
UPDATE Comands SET CountreFK = 1+ rand(checksum(newid()))*232;
UPDATE Comands SET Name = (SELECT C.Name_Countries FROM Countries C WHERE C.Id_Countries =CountreFK);

SELECT * FROM Comands;

EXEC Insert�hampionship;

TRUNCATE TABLE Groups;

EXEC PlayChampoinship;
GO



EXEC PlayChampoinship 

--EXEC Replay 
GO

EXEC SELECTS;
GO

