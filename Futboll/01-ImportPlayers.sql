USE Football

-- �������� ������� ����� ��
CREATE TABLE Players4
(
    Id INT PRIMARY KEY IDENTITY,
    Position NCHAR(2),
    Name NVARCHAR(30)
)
GO

-- ��������� ������� ��� �������. ���� ����� ������������� ����� � ������������� ��������� �����.
CREATE TABLE PlayersTemp
(
    Position NCHAR(2),
    Name NVARCHAR(30)
)
GO

-- ����������� �� ��������� ������� ������ �� ���������� ����� 
BULK INSERT PlayersTemp
FROM 'C:\Users\Lyubov\itstep-bpu-1821\03-DB\Homework\�������\��������� ���� �� �������\������.txt'
WITH (FIELDTERMINATOR = '\t', ROWTERMINATOR = '\n')
GO

-- �������� ������ �� ��������� ������� � ��������. ��� ���� ��������� ����
-- � �������� ������� ����� �������������� ������������� (�� �� �������� ��� IDENTITY)
INSERT INTO Players4 (Position, Name)
SELECT Position, Name
FROM PlayersTemp
GO

-- ������� ����� �� ������ ��������� �������
DROP TABLE PlayersTemp
GO

-- ���������
SELECT * FROM Players
GO
