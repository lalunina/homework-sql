﻿

IF DB_ID('Football') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE Football SET single_user with rollback immediate;
    DROP DATABASE Football;
END
GO

CREATE DATABASE Football;
GO


USE Football
GO


CREATE TABLE Play_Off
(
	Id int NOT NULL PRIMARY KEY,
	Name varchar(50)
)
GO


CREATE TABLE Сontinents
(
	Id int NOT NULL PRIMARY KEY,
	Name varchar(50)
)
GO



 --associations Футбольная ассоциация

 CREATE TABLE Associations
(
	Id int NOT NULL PRIMARY KEY,
	Name varchar(50)
)
GO


--team positions - позиция в команде
 CREATE TABLE Positions
(
	Id int NOT NULL PRIMARY KEY,
	
    Abbreviation varchar(2),
	Name varchar(50)
)
GO

CREATE TABLE Еquipment
(
    Id int NOT NULL PRIMARY KEY,
	
	Name varchar(50)
)
GO

INSERT Play_Off (Id, Name) VALUES (1, 'Отборочный')
INSERT Play_Off (Id, Name) VALUES (2, '1/8')
INSERT Play_Off (Id, Name) VALUES (3, '1/4')
INSERT Play_Off (Id, Name) VALUES (4, '1/2')
INSERT Play_Off (Id, Name) VALUES (5, 'Игра за третье место')
INSERT Play_Off (Id, Name) VALUES (6, 'Финал')

INSERT Сontinents(Id, Name) VALUES (1, N'Европа')
INSERT Сontinents(Id, Name) VALUES (2, N'Азия')
INSERT Сontinents(Id, Name) VALUES (3, N'Северная америка')
INSERT Сontinents(Id, Name) VALUES (4, N'Южная америка')
INSERT Сontinents(Id, Name) VALUES (5, N'Африка')
INSERT Сontinents(Id, Name) VALUES (6, N'Австралия')

INSERT  Associations (Id, Name) VALUES (1, N'АФК')
INSERT  Associations (Id, Name) VALUES (2, N'КАФ')
INSERT  Associations (Id, Name) VALUES (3, N'КОНКАКАФ')
INSERT  Associations (Id, Name) VALUES (4, N'КОНМЕБОЛ')
INSERT  Associations (Id, Name) VALUES (5, N'ОФК')
INSERT  Associations (Id, Name) VALUES (6, N'УЕФА')


--Team_positions
INSERT  Positions (Id, Abbreviation, Name) VALUES (1,'GK', N'Вратарь')
INSERT  Positions (Id, Abbreviation, Name) VALUES (2,'DF',  N'Защитник')
INSERT  Positions (Id, Abbreviation, Name) VALUES (3,'MF', N'Полузащитник')
INSERT  Positions (Id, Abbreviation, Name) VALUES (4,'FW', N'Наподающий')

INSERT  Еquipment (Id, Name) VALUES (1, N'Adidas')
INSERT  Еquipment (Id, Name) VALUES (2, N'Nike')
INSERT  Еquipment (Id, Name) VALUES (3, N'Joma')
INSERT  Еquipment (Id, Name) VALUES (4, N'Kappa')
INSERT  Еquipment (Id, Name) VALUES (5, N'Lotto')
INSERT  Еquipment (Id, Name) VALUES (7, N'New Balance')
INSERT  Еquipment (Id, Name) VALUES (8, N'Umbro')
INSERT  Еquipment (Id, Name) VALUES (9, N'Under Armor')

GO



CREATE TABLE Countries
(
   Name_Countries nvarchar(50),
   Id_Countries bigint IDENTITY NOT NULL PRIMARY KEY,

)
GO


BULK INSERT Countries
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\countries.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n')
GO

--SELECT * FROM Countries
--GO

CREATE TABLE Players
(
    ID int NOT NULL PRIMARY KEY,
	Number int,
    Name NVARCHAR(30),
	ComanFK int,
	PozitionFK int
)
GO

BULK INSERT Players
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\Players.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n')
GO

SELECT * FROM Players
GO


CREATE TABLE Comands
(
    ID int NOT NULL PRIMARY KEY,
    Name NVARCHAR(30),
	Triner NVARCHAR(30),
	CountreFK int,
	EquipmentFK int
)
GO


-- Импортируем в таблицу записи из  файла csv
BULK INSERT Comands
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\CopiComands.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n', CODEPAGE = 'ACP')
GO

CREATE TABLE Referees
(
    Id int NOT NULL PRIMARY KEY,
	AssociationFK int,
	CountrieFK int,
	Name varchar(50)
)

BULK INSERT Referees
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\referee.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n',CODEPAGE = 'ACP')
GO

SELECT * FROM Referees
GO

CREATE TABLE Stadiums
(
    Id int NOT NULL PRIMARY KEY,
	NameSity varchar(50),
	NameStadium varchar(50),
	Capacity int

)

BULK INSERT Stadiums
FROM 'C:\Users\Lyubov\DZ_Lunina\homework-sql\Futboll\Studiums.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n',CODEPAGE = 'ACP')
GO

SELECT * FROM Stadiums
GO



CREATE TABLE Qualifying_Matchs
(
    Id int NOT NULL PRIMARY KEY,
	
	Team1IDFK int,
	Team2IDFK int,
	Group_N int
)

BULK INSERT Qualifying_Matchs
FROM 'C:\Users\Lyubov\homework-sql\Futboll\Qualifying_results.csv'
WITH (FORMAT='CSV', FIELDTERMINATOR = '\;', ROWTERMINATOR = '\n',CODEPAGE = 'ACP')
GO

ALTER TABLE Qualifying_Matchs ADD
    Goals1 int ,
    Goals2 int,
	Points1 int,
	Points2 int,
    RefereesFK int,
    StadiumFK int,
    Viewers int;

SELECT * FROM Qualifying_Matchs
GO


CREATE TABLE Groups 
(ID int IDENTITY,
 GroupNumber int,
 NumberInGroup int, --номер в группе 
 ComandFK  int
)

--SELECT * FROM Comands
--GO


CREATE TABLE Сhampionship
(ID INT IDENTITY,
	Teams1 int,
	Teams2 int,
	PlayOfFK int,
	Goals1 int DEFAULT NULL,
    Goals2 int DEFAULT NULL,
	Penalty1 int DEFAULT NULL,
	Penalty2 int DEFAULT NULL, 
	Points1 int DEFAULT NULL,
	Points2 int DEFAULT NULL,
    RefereesFK int DEFAULT NULL,
    StadiumFK int DEFAULT NULL,
    Viewers int DEFAULT NULL

)
--SELECT* FROM Сhampionship;

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InsertСhampionship' AND type = 'P')
    DROP PROCEDURE InsertСhampionship;
GO

CREATE PROCEDURE InsertСhampionship
AS
--BEGIN
TRUNCATE TABLE  Сhampionship;
--END;
--BEGIN
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (1, 3, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (2, 4, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (5, 7, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (6, 8, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (9, 11, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (10, 12, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (13, 15, 2);
INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (14, 16, 2);


 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (1, 2, 3);
 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (3, 4, 3);
 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (5, 6, 3);
 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (7, 8, 3);

 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (1, 2, 4);
 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (3, 4, 4);
 --END;
 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (1, 2, 5);
 INSERT INTO Сhampionship (Teams1,Teams2, PlayOfFK) VALUES (1, 2, 6);

SELECT* FROM Сhampionship;
GO

EXEC InsertСhampionship;
GO