--��������� � �������

--������ ������������ �������� �������� ��� �������:
--1. ����������� � ���������, ������� �������� ����������� ��� ������ � ����������, � ��������� ������� ��������� ������ ������ ���������� �������. ������ ���� ��� ��������� ��������� ����������_���������_������
--2. ����������_���������_������ � �������������� ��������������� �������. ������ ���� ��������� ��������� �����������_1_8
--3. �����������_1_8 � ����� ��� �� �������������� ����������� ������� � ��������� ��������� ��������� ��� ���������� �����.
--4. � �����, ����� ��� ��� ��, ������ �� ������� ����� ����� ������� � ����� ������ � ������������� ���������� ������.


--�������� ��������� �������:
--1. ����� ���������� �����, ������� �� ���������� ����������
--2. ������� ���������� ����� � ������ �����
--3. ���������� � ���������� ���������� �����, ������� ��������� (2 �������) \\ ���������
--4. ���������� � ���������� ���������� �����, ����������� ��������� (2 �������)
--5. ��������� ������������ ���� ������ \\ ��������� �����
--6. ������� ������������ ������ ����� \\ 
--7. ���������� � ���������� ����� ����� (2 ������������ �������) \\??
--8. ���������� � ���������� ���������� ��������� (2 �������)
--9. ������ ���������� (������, �������� ���������� ���������� �����)
--10. ������ ������, �������� �������� �����
--������� ������ � ����������. (�hampionship_Data)
--
--���������� 8 �����, 32 �������, ������ ������ �� 3 �����, ��� ������ ������� ������ ������� � 1\8

--1\8 8 ��� (16 ������) 
--1\4 4 ���� (8 �����)
--1\2 2 ���� (4 �������) ���������� ������� � �����, ����������� �� ������ �����
--�����   ���������� ��� 1\2
--���� �� ������ ����� ����������� ��� 1\2 



--�����������. ������ ���� ��� ��������� ��������� ����������_���������_������ 
--���������, ����.
--����������, 48 ���,�����, ��������,
--���������� ��� 48 ���
-- �� ����� �������� �������, ����.\\ ��������������� ������ �� ����������� �������� ����

--���� - ���������� �� 0 �� 3

--\\�������  ��������� ����������� ������� �������, �� ��� ����� �������� �� �������� � ����������\\
--\\��������� - ����������� �� ��������� �� ��������� ������� ������� � ��� ����������� �����\\



--�� ����� ��������� ������� - ����������_���������_������ , ���� �����������:
--ID
--�������1
--�������2
--��������������� 1
--������� �������� 2
--����������� ����:
--!������ � ����� ������� � ����� �������� �� ������� QUAL_MATCHS - ������ ������ ������ 6 ������ => �� ������� ������, ������ 6 � ����� ������. ���� ������������, � ����������� ������.
-- ������� ����� - ���� ( ������� ������ ��� ����������� - 3 ����, ����� - 1 ���, ������ - 0 �����) 

--�������
--���������� �� �������� �������� ��������� ������ �������, 
--���������� �� �������, � ������� ������ �� ������ 



-- ���������� �� � ���� 1\8 
-- ������ � ���������,
-- �������� ��������� ���������� �����, ���������� �� �����������, ���� �����, �������� ������ ���������� �� �������� 
-- + ������� �����
-- ���� ������  - 1 �������� -0 
-- ��������� ��������� � ������� 1\4
-- ������ � ���������,
-- �������� ��������� ���������� �����, ���������� �� �����������, ���� �����, �������� ������ ���������� �� �������� 
-- + ������� �����
--������ � 1\2
--�� 1\2 ���������� � �����
--����������� � ���� �� ������ �����
-- ����� ������� ���������� � ������� ����� ��� 1\8, 1\4, 1\2 , ������ � ����������.

-- ������������� - ���������
-- ������� ������� ����������� 

--������� �����������

-- ��������� 
-- ������� ����������.
--CREATE TABLE Participants 
--(
--   ID int NOT NULL PRIMARY KEY,
--    Name NVARCHAR(30),
--	Triner NVARCHAR(30),
--	CountreFK int,
--	EquipmentFK int,
--
--)

--CREATE TABLE Groups 
--(ID int IDENTITY NOT NULL PRIMARY KEY,
-- GroupNumber int,
-- NumberInGroup int, --����� � ������ 
-- ComandFK  int
--)
USE Football;

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InsertGroups' AND type = 'P')
    DROP PROCEDURE InsertGroups;
GO

CREATE PROCEDURE InsertGroups 
AS
  DECLARE @i int , @a int, @b int
  SET @i=1
  SET @b=1

 While @b<=8
BEGIN
	  SET @a=1 
	 While @a<=4
BEGIN
        insert into Groups ( NumberInGroup,GroupNumber, ComandFK ) values (@a, @b, @i)
		SET @i=@i+1
        SET @a=@a+1 
END
   SET @b=@b+1   
END
   Set NoCount OFF

  --SELECT * FROM Groups;
GO



EXEC InsertGroups
GO



--TRUNCATE TABLE Groups;
SELECT * FROM Groups;


--CREATE TABLE main_cast
--(ID INT IDENTITY
--
--
--)


USE Football;

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'UP_Qualifying_Matchs' AND type = 'P')
    DROP PROCEDURE UP_Qualifying_Matchs;
GO

CREATE PROCEDURE UP_Qualifying_Matchs
AS
	UPDATE Qualifying_Matchs SET Goals1 = rand(checksum(newid()))*3;
	UPDATE Qualifying_Matchs SET Goals2 = rand(checksum(newid()))*3;
	UPDATE Qualifying_Matchs SET RefereesFK = 1+ rand(checksum(newid()))*29;  
	UPDATE Qualifying_Matchs SET StadiumFK =1+ rand(checksum(newid()))*12;
	UPDATE Qualifying_Matchs SET Viewers = 3000+rand(checksum(newid()))*(SELECT Stadiums.Capacity - 3000 FROM Stadiums WHERE Stadiums.Id = Qualifying_Matchs.StadiumFK);

	UPDATE Qualifying_Matchs SET Points1 = 3 WHERE Goals1 > Goals2;
    UPDATE Qualifying_Matchs SET Points1 = 1 WHERE Goals1 = Goals2;
	UPDATE Qualifying_Matchs SET Points1 = 0 WHERE Goals1 < Goals2;
	UPDATE Qualifying_Matchs SET Points2 = 3 WHERE Goals1 < Goals2;
    UPDATE Qualifying_Matchs SET Points2 = 1 WHERE Goals1 = Goals2;
	UPDATE Qualifying_Matchs SET Points2 = 0 WHERE Goals1 > Goals2;

	--UPDATE Qualifying_Matchs SET Team1IDFK =  Groups.ComandFK From Groups
    --WHERE Groups.NumberInGroup IN (SELECT Qualifying_Matchs.Team1IDFK FROM Qualifying_Matchs)
    --AND Groups.GroupNumber IN ( SELECT Qualifying_Matchs.Group_N FROM Qualifying_Matchs);

	--UPDATE Qualifying_Matchs SET Team2IDFK = (Select Groups.ComandFK From Groups
    --WHERE Groups.NumberInGroup IN (SELECT Qualifying_Matchs.Team2IDFK FROM Qualifying_Matchs)
    --AND Groups.GroupNumber IN ( SELECT Qualifying_Matchs.Group_N FROM Qualifying_Matchs));

SELECT * FROM Qualifying_Matchs;
GO

EXEC UP_Qualifying_Matchs
GO

USE Football;



Drop table UP_T1;

	CREATE table UP_T1 

   (ID int,
	Teams1 int,
	Teams2 int,
	Group_N int,
	Goals1 int,
    Goals2 int,
	Points1 int,
	Points2 int,
    RefereesFK int,
    StadiumFK int,
    Viewers int
	)
	
	--Insert  INTO  UP_T1  SELECT * From Qualifying_Matchs;
	
	SELECT * FROM UP_T1;

	 

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'UP_Teams' AND type = 'P')
    DROP PROCEDURE UP_Teams;
GO

CREATE PROCEDURE UP_Teams
AS
	
Insert  INTO  UP_T1  SELECT * From Qualifying_Matchs;

--UPDATE UP_T1 SET Teams1 =
--(Select Groups.ComandFK From Groups
--WHERE Groups.NumberInGroup IN (SELECT UP_T1.Teams1 FROM Qualifying_Matchs)
--AND Groups.GroupNumber IN ( SELECT UP_T1.Group_N FROM Qualifying_Matchs));
--
--UPDATE UP_T1 SET Teams2 =
--(Select Groups.ComandFK From Groups
--WHERE Groups.NumberInGroup IN (SELECT UP_T1.Teams2 FROM Qualifying_Matchs)
--AND Groups.GroupNumber IN ( SELECT UP_T1.Group_N FROM Qualifying_Matchs));

S

EXEC UP_Teams 
GO
SELECT*  FROM UP_T1;
CREATE VIEW  Results_Q
AS
SELECT  ID, Teams2 AS Teams, Group_N AS Group_N,  Points2 AS Point
FROM UP_T1
--GROUP BY  Group_N, Teams2, ID
UNION
SELECT  ID, Teams1 AS Teams, Group_N AS Group_N,  Points1 AS Point
FROM UP_T1;
--GROUP BY  Group_N, Teams1, ID --ORDER BY Points DESC;
GO

SELECT*  FROM Results_Q;
DROP VIEW  Results_Q_S;

CREATE VIEW  Results_Q_S
AS
SELECT  Teams, Group_N, SUM (Point) AS Points
FROM Results_Q
GROUP BY  Teams, Group_N;

SELECT*  FROM Results_Q_S;


--SELECT  Teams, Group_N, SUM (Point) AS Points
--INTO #Order
--FROM Results_Q 
--GROUP BY  Teams, Group_N
--ORDER BY Points DESC;



 

 --INSERT  INTO one_eighth SET Teams1 = 




IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InsertOneEighth' AND type = 'P')
    DROP PROCEDURE InsertOneEighth;
GO

CREATE PROCEDURE InsertOneEighth 
AS

--SELECT * FROM UP_T1;

DECLARE @table TABLE
(ID int IDENTITY ,Points int, Teams int, Group_N int )

DECLARE @i int, @x int;
SET @i=1;

WHILE @i <9

BEGIN

INSERT INTO @table  SELECT TOP (2) Points, Teams, Group_N
 FROM Results_Q_S
 WHERE  Group_N =@i
 ORDER BY Points DESC;

 SET @i=@i+1;
  
 END
 
 SELECT * FROM @table;
 

 UPDATE one_eighth SET Teams1 = (Select t.Teams From @table t WHERE ID = Teams1);
 UPDATE one_eighth SET Teams2 = (Select t.Teams From @table t WHERE ID = Teams2); 

 UPDATE one_eighth SET Goals1 = rand(checksum(newid()))*3;
 UPDATE one_eighth SET Goals2 = rand(checksum(newid()))*3;
 UPDATE one_eighth SET RefereesFK = 1+ rand(checksum(newid()))*29;  
 UPDATE one_eighth SET StadiumFK =1+ rand(checksum(newid()))*12;
 UPDATE one_eighth SET Viewers = 3000+rand(checksum(newid()))*(SELECT Stadiums.Capacity - 3000 FROM Stadiums WHERE Stadiums.Id = one_eighth.StadiumFK);

	UPDATE one_eighth SET Points1 = 3 WHERE Goals1 > Goals2;
    UPDATE one_eighth SET Points1 = 1 WHERE Goals1 = Goals2;
	UPDATE one_eighth SET Points1 = 0 WHERE Goals1 < Goals2;
	UPDATE one_eighth SET Points2 = 3 WHERE Goals1 < Goals2;
    UPDATE one_eighth SET Points2 = 1 WHERE Goals1 = Goals2;
	UPDATE one_eighth SET Points2 = 0 WHERE Goals1 > Goals2;


CREATE View SPOR
AS
SELECT * FROM one_eighth WHERE Points1 = 1;

 --SELECT * FROM SPOR;

 UPDATE SPOR SET Penalty1 = rand(checksum(newid()))*2;
 UPDATE SPOR SET Penalty2 = rand(checksum(newid()))*2;

 EXEC InsertOneEighth 
GO


CREATE View SPOR
AS
SELECT * FROM one_eighth WHERE Points1 = 1;

 SELECT * FROM SPOR;

 UPDATE SPOR SET Penalty1 = rand(checksum(newid()))*2;
 UPDATE SPOR SET Penalty2 = rand(checksum(newid()))*2;

 SELECT * FROM one_eighth;




--�����������

--IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Replay' AND type = 'P')
--    DROP PROCEDURE Replay;
--GO
--
--
--CREATE PROCEDURE Replay
--AS 
--UPDATE Comands SET CountreFK = 1+ rand(checksum(newid()))*232;
--UPDATE Comands SET Name = (SELECT Name FROM Countries WHERE CountreFK IN (SELECT ID FROM Countries));
--
--EXEC UP_Teams 
--GO
--
--EXEC Replay 
--GO

--!!
--DESCRIBE  Qualifying_Matchs;


--DROP VIEW Test3 
--CREATE VIEW Test3 
--AS  
--SELECT  C.Triner as TrinerFK ,  C.EquipmentFK as EquipmentFK, CO.Id_Countries as Country  
--FROM Countries CO, Comands C
----WHERE Country IN (SELECT TOP 30 Id_Countries
----FROM Countries
----ORDER BY NEWID())
----GO
-- SELECT * FROM Test3
-- DECLARE @i int
--	SET @i =1




--SELECT *
--FROM Test3;
--GO
--
--;
--
---- ��������� ��� ������ �������
--SELECT TOP 11 ID 
--FROM Players 
--WHERE ComanFK = 11
--ORDER BY NEWID())
--GO
--
--SELECT *
--FROM Test;
--GO
--


--���� ������� ������ ������ � �������� ������� �� ���������� ����� � ����������� ��������� ������ �� ��� �� ������� 
--�������� ��������� � ���������� �������, ���������� ���� ������� ���������� ����� �� ���������� ������� �����, 
--������� ���� ������� ���������� ��
