-- ������� ���� ������

IF DB_ID('HRBase') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE HRBase SET single_user with rollback immediate
    DROP DATABASE  HRBase 
END
GO

CREATE DATABASE HRBase
GO

USE HRBase
GO

CREATE TABLE Posts
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,

)
GO
CREATE TABLE Fotos
(
Id bigint  NOT NULL PRIMARY KEY,
FileName  nvarchar, 
Title nvarchar, 
ImageData varbinary(MAX),--- ����� ��������� �������� ������ ����� � ����� ��� 
)
CREATE TABLE Staf
(
    Id bigint NOT NULL IDENTITY PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	FirstName nvarchar(50) NOT NULL,
	LastName nvarchar(50) NOT NULL,
	DateBirth date,
	PostFK bigint,
	N_Recruitment bigint, --����� ������� �� ������ �� ������
	N_Dismissal   bigint, --����� ������� �� ����������
	--FotoFK bigint,

	FOREIGN KEY (PostFK) REFERENCES Posts(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE, 

	--FOREIGN KEY (FotoFK) REFERENCES Fotos(Id)
	--    ON DELETE CASCADE
    --    ON UPDATE CASCADE,
)
GO

INSERT INTO Posts VALUES (1, N'��������');
INSERT INTO Posts VALUES (2, N'��������');
INSERT INTO Posts VALUES (3, N'��������');
INSERT INTO Posts VALUES (4, N'�������');
INSERT INTO Posts VALUES (5, N'������');
INSERT INTO Posts VALUES (6, N'������� ��������');
GO


INSERT INTO Staf VALUES (N'����',   N'�������', '2001-01-01',1, 1, 2);

INSERT INTO Staf VALUES (N'����',   N'������������', '2002-08-01',2, 0, 3);
INSERT INTO Staf VALUES (N'������',   N'���', '2000-07-01',1, 0, 3);

GO

SELECT *
FROM Staf;
GO