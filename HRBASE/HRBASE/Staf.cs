﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace HRBASE
{
    public partial class Staf : Form
    {
        private List<long> PostsIDs = new List<long>();
        public Staf()
        {
            InitializeComponent();
        }

        private void Posts_Load(object sender, EventArgs e)
        {
            try
            {
                SqlCommand comm = HRBASE.connection.CreateCommand();
                comm.CommandText = "SELECT ID, Name  FROM Posts";

                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    PostsIDs.Add(reader.GetInt64(reader.GetOrdinal("ID")));
                    comboBoxPosts.Items.Add(reader.GetString(reader.GetOrdinal("Name")));
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

            ActiveControl = comboBoxPosts;
        }


        private long postFk = 0;

        public long PostFk
        {
            get
            {
                return PostsIDs[comboBoxPosts.SelectedIndex];
            }

            set
            {
                postFk = value;
            }
        }

        public string PostName
        {
            get
            {
                return comboBoxPosts.Items[comboBoxPosts.SelectedIndex].ToString();
            }
        }

        public string PersonalName
        {
            get
            {
                return textBoxName.Text;
            }

            set
            {
                textBoxName.Text = value;
            }
        }

       public string PersonalFName
       {
           get
           {
                return textboxLastName.Text;
             
           }
       
           set
           {
                 textboxLastName.Text = value;
            }
        }

        public int NumerPP
        {
            get
            {
                  return Convert.ToInt32(textPP.Text);

            }

            set
            {
                textPP.Text = value.ToString();
            }
        }


        public int NumerPU
        {
            get
            {
                return Convert.ToInt32(textPU.Text);

            }

            set
            {
                textPU.Text = value.ToString();
            }
        }

        public DateTime BirthData
        {
            get
            {
                return Convert.ToDateTime(textboxBirthData.Text);

            }

            set
            {
                textboxBirthData.Text = value.ToString();
            }
        }


        private void SfafR_Shown(object sender, EventArgs e)
        {
            comboBoxPosts.SelectedIndex = PostsIDs.IndexOf(postFk);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (PersonalName.Trim().Length == 0)
                MessageBox.Show("Необходимо заполнить поле 'Название'.", "Не все поля заполнены", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                DialogResult = DialogResult.OK;
        }

        
    }
}


