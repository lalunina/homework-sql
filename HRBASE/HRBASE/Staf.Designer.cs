﻿using System.Windows.Forms;

namespace HRBASE
{
    partial class Staf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxStaf = new System.Windows.Forms.GroupBox();
            this.textboxBirthData = new System.Windows.Forms.TextBox();
            this.labelBirthData = new System.Windows.Forms.Label();
            this.pictureBoxFoto = new System.Windows.Forms.PictureBox();
            this.labelFoto = new System.Windows.Forms.Label();
            this.textPU = new System.Windows.Forms.TextBox();
            this.labelPU = new System.Windows.Forms.Label();
            this.textPP = new System.Windows.Forms.TextBox();
            this.labelPP = new System.Windows.Forms.Label();
            this.comboBoxPosts = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textboxLastName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelLastName = new System.Windows.Forms.Label();
            this.labelPost = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.imageFoto = new System.Windows.Forms.ImageList(this.components);
            this.groupBoxStaf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFoto)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxStaf
            // 
            this.groupBoxStaf.Controls.Add(this.textboxBirthData);
            this.groupBoxStaf.Controls.Add(this.labelBirthData);
            this.groupBoxStaf.Controls.Add(this.pictureBoxFoto);
            this.groupBoxStaf.Controls.Add(this.labelFoto);
            this.groupBoxStaf.Controls.Add(this.textPU);
            this.groupBoxStaf.Controls.Add(this.labelPU);
            this.groupBoxStaf.Controls.Add(this.textPP);
            this.groupBoxStaf.Controls.Add(this.labelPP);
            this.groupBoxStaf.Controls.Add(this.comboBoxPosts);
            this.groupBoxStaf.Controls.Add(this.textBoxName);
            this.groupBoxStaf.Controls.Add(this.textboxLastName);
            this.groupBoxStaf.Controls.Add(this.labelName);
            this.groupBoxStaf.Controls.Add(this.labelLastName);
            this.groupBoxStaf.Controls.Add(this.labelPost);
            this.groupBoxStaf.Location = new System.Drawing.Point(12, 12);
            this.groupBoxStaf.Name = "groupBoxStaf";
            this.groupBoxStaf.Size = new System.Drawing.Size(666, 281);
            this.groupBoxStaf.TabIndex = 1;
            this.groupBoxStaf.TabStop = false;
            this.groupBoxStaf.Text = "Информация по сотруднику";
            // 
            // textboxBirthData
            // 
            this.textboxBirthData.Location = new System.Drawing.Point(197, 233);
            this.textboxBirthData.Name = "textboxBirthData";
            this.textboxBirthData.Size = new System.Drawing.Size(282, 20);
            this.textboxBirthData.TabIndex = 10;
          
            // 
            // labelBirthData
            // 
            this.labelBirthData.AutoSize = true;
            this.labelBirthData.Location = new System.Drawing.Point(25, 236);
            this.labelBirthData.Name = "labelBirthData";
            this.labelBirthData.Size = new System.Drawing.Size(89, 13);
            this.labelBirthData.TabIndex = 9;
            this.labelBirthData.Text = "Дата рождения:";
            // 
            // pictureBoxFoto
            // 
            this.pictureBoxFoto.Location = new System.Drawing.Point(525, 44);
            this.pictureBoxFoto.Name = "pictureBoxFoto";
            this.pictureBoxFoto.Size = new System.Drawing.Size(100, 142);
            this.pictureBoxFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxFoto.TabIndex = 8;
            this.pictureBoxFoto.TabStop = false;
            // 
            // labelFoto
            // 
            this.labelFoto.AutoSize = true;
            this.labelFoto.Location = new System.Drawing.Point(537, 27);
            this.labelFoto.Name = "labelFoto";
            this.labelFoto.Size = new System.Drawing.Size(41, 13);
            this.labelFoto.TabIndex = 7;
            this.labelFoto.Text = "ФОТО";
            // 
            // textPU
            // 
            this.textPU.Location = new System.Drawing.Point(197, 191);
            this.textPU.Name = "textPU";
            this.textPU.Size = new System.Drawing.Size(282, 20);
            this.textPU.TabIndex = 6;
            // 
            // labelPU
            // 
            this.labelPU.AutoSize = true;
            this.labelPU.Location = new System.Drawing.Point(25, 194);
            this.labelPU.Name = "labelPU";
            this.labelPU.Size = new System.Drawing.Size(166, 13);
            this.labelPU.TabIndex = 5;
            this.labelPU.Text = "Номер приказа об увольнении:";
            // 
            // textPP
            // 
            this.textPP.Location = new System.Drawing.Point(197, 148);
            this.textPP.Name = "textPP";
            this.textPP.Size = new System.Drawing.Size(282, 20);
            this.textPP.TabIndex = 4;
            // 
            // labelPP
            // 
            this.labelPP.AutoSize = true;
            this.labelPP.Location = new System.Drawing.Point(25, 151);
            this.labelPP.Name = "labelPP";
            this.labelPP.Size = new System.Drawing.Size(139, 13);
            this.labelPP.TabIndex = 3;
            this.labelPP.Text = "Номер приказа о приеме:";
            // 
            // comboBoxPosts
            // 
            this.comboBoxPosts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPosts.FormattingEnabled = true;
            this.comboBoxPosts.Location = new System.Drawing.Point(124, 27);
            this.comboBoxPosts.Name = "comboBoxPosts";
            this.comboBoxPosts.Size = new System.Drawing.Size(355, 21);
            this.comboBoxPosts.TabIndex = 0;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(124, 56);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(355, 20);
            this.textBoxName.TabIndex = 1;
            // 
            // textboxLastName
            // 
            this.textboxLastName.Location = new System.Drawing.Point(124, 99);
            this.textboxLastName.Name = "textboxLastName";
            this.textboxLastName.Size = new System.Drawing.Size(355, 20);
            this.textboxLastName.TabIndex = 2;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(25, 60);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(32, 13);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Имя:";
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(25, 99);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(59, 13);
            this.labelLastName.TabIndex = 0;
            this.labelLastName.Text = "Фамилия:";
            // 
            // labelPost
            // 
            this.labelPost.AutoSize = true;
            this.labelPost.Location = new System.Drawing.Point(25, 30);
            this.labelPost.Name = "labelPost";
            this.labelPost.Size = new System.Drawing.Size(68, 13);
            this.labelPost.TabIndex = 0;
            this.labelPost.Text = "Должность:";
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.Location = new System.Drawing.Point(512, 299);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(78, 25);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(600, 299);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(78, 25);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // imageFoto
            // 
            this.imageFoto.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageFoto.ImageSize = new System.Drawing.Size(25, 25);
            this.imageFoto.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // Staf
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(690, 336);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBoxStaf);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Staf";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактирование информации о книге";
            this.Load += new System.EventHandler(this.Posts_Load);
            this.Shown += new System.EventHandler(this.SfafR_Shown);
            this.groupBoxStaf.ResumeLayout(false);
            this.groupBoxStaf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxStaf;
        private System.Windows.Forms.ComboBox comboBoxPosts;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textboxLastName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.Label labelPost;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private Label labelPP;
        private Label labelFoto;
        private TextBox textPU;
        private Label labelPU;
        private TextBox textPP;
        private ImageList imageFoto;
        private PictureBox pictureBoxFoto;
        private TextBox textboxBirthData;
        private Label labelBirthData;
    }
}