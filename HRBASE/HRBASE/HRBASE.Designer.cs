﻿using System.Windows.Forms;
using System.Drawing;

namespace HRBASE
{
    partial class HRBASE
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.toolStripFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripFileExitItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripHelpItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripHelpAboutItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listView = new System.Windows.Forms.ListView();
            this.columnAuthor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnL_Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNumber_PP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnNumber_PU = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnBirthData = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFileItem,
            this.toolStripHelpItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(121, 24);
            this.menuStripMain.TabIndex = 1;
            this.menuStripMain.Text = "Главное меню";
            // 
            // toolStripFileItem
            // 
            this.toolStripFileItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFileExitItem});
            this.toolStripFileItem.Name = "toolStripFileItem";
            this.toolStripFileItem.Size = new System.Drawing.Size(48, 20);
            this.toolStripFileItem.Text = "&Файл";
            // 
            // toolStripFileExitItem
            // 
            this.toolStripFileExitItem.Name = "toolStripFileExitItem";
            this.toolStripFileExitItem.Size = new System.Drawing.Size(109, 22);
            this.toolStripFileExitItem.Text = "В&ыход";
            this.toolStripFileExitItem.Click += new System.EventHandler(this.toolStripFileExitItem_Click);
            // 
            // toolStripHelpItem
            // 
            this.toolStripHelpItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripHelpAboutItem});
            this.toolStripHelpItem.Name = "toolStripHelpItem";
            this.toolStripHelpItem.Size = new System.Drawing.Size(65, 20);
            this.toolStripHelpItem.Text = "&Справка";
            // 
            // toolStripHelpAboutItem
            // 
            this.toolStripHelpAboutItem.Name = "toolStripHelpAboutItem";
            this.toolStripHelpAboutItem.Size = new System.Drawing.Size(149, 22);
            this.toolStripHelpAboutItem.Text = "О программе";
            this.toolStripHelpAboutItem.Click += new System.EventHandler(this.toolStripHelpAboutItem_Click);
            // 
            // listView
            // 
            this.listView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnAuthor,
            this.columnName,
            this.columnL_Name,
            this.columnNumber_PP,
            this.columnNumber_PU,
            this.columnBirthData});
            this.listView.FullRowSelect = true;
            this.listView.GridLines = true;
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(21, 27);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(630, 389);
            this.listView.TabIndex = 2;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_MouseDoubleClick);
            // 
            // columnAuthor
            // 
            this.columnAuthor.Text = "Должность";
            this.columnAuthor.Width = 97;
            // 
            // columnName
            // 
            this.columnName.Text = "Имя";
            this.columnName.Width = 92;
            // 
            // columnL_Name
            // 
            this.columnL_Name.Text = "Фамилия";
            this.columnL_Name.Width = 108;
            // 
            // columnNumber_PP
            // 
            this.columnNumber_PP.Text = "Приказ о приеме";
            this.columnNumber_PP.Width = 105;
            // 
            // columnNumber_PU
            // 
            this.columnNumber_PU.Text = "Приказ об увольнении";
            this.columnNumber_PU.Width = 132;
            // 
            // columnBirthData
            // 
            this.columnBirthData.Text = "Дата Рождения";
            this.columnBirthData.Width = 98;
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAdd.Location = new System.Drawing.Point(657, 27);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(94, 23);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit.Location = new System.Drawing.Point(657, 56);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(94, 23);
            this.buttonEdit.TabIndex = 3;
            this.buttonEdit.Text = "Изменить";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(657, 85);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(94, 23);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // HRBASE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 428);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.menuStripMain);
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "HRBASE";
            this.Text = "РЕЕСТР";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.ToolStripMenuItem toolStripFileItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripHelpItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripFileExitItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripHelpAboutItem;
        private System.Windows.Forms.ColumnHeader columnAuthor;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnL_Name;
        
        private System.Windows.Forms.ColumnHeader columnNumber_PP;
        private System.Windows.Forms.ColumnHeader columnNumber_PU;
        private System.Windows.Forms.ColumnHeader columnBirthData;
    }
}

