﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRBASE
{
    public partial class HRBASE : Form
    {
        public static SqlConnection connection = new SqlConnection();// 
        public static List<long> SIDs = new List<long>();
        public static List<long> PostFKs = new List<long>();


        public HRBASE()
        {
            InitializeComponent();
        }
        private void toolStripFileExitItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void toolStripHelpAboutItem_Click(object sender, EventArgs e)
        {//
            //(new FormAbout()).ShowDialog();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = "(local)";      // Сервер
            builder.InitialCatalog = "HRBase";  // Текущая БД
            builder.IntegratedSecurity = true;   // Режим проверки подлинности

            connection.ConnectionString = builder.ConnectionString;

            try
            {
                connection.Open();
               FillListView();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                Close();
            }
        }

        private void FillListView()
        {
            try
            {
                string sqlString =
                        "SELECT Staf.ID, Staf.PostFK as PostFK, Staf.FirstName AS Name, Staf.LastName AS LastName, Staf.DateBirth AS DateBirth, Staf.N_Recruitment AS NumberPP, Staf.N_Dismissal AS NumberPU, Posts.Name AS PostName "
                        +
                        "FROM  Staf, Posts "
                        + "WHERE Staf.PostFK =  Posts.Id ";

                

                using (SqlCommand command = new SqlCommand(sqlString, connection))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        SIDs.Add((long)reader["Id"]);
                        PostFKs.Add((long)reader["PostFK"]);

                        ListViewItem item = listView.Items.Add(new ListViewItem());
                        item.Text = (string)reader["PostName"];
                      
                        item.SubItems.Add((string)reader["Name"]);
                        item.SubItems.Add((string)reader["LastName"]);
                        item.SubItems.Add(((long)reader["NumberPP"]).ToString());
                        item.SubItems.Add(((long)reader["NumberPU"]).ToString());
                        item.SubItems.Add(((DateTime)reader["DateBirth"]).ToString());
                        
                    }
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (connection.State == ConnectionState.Open)
                connection.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var dlg = new Staf();

            dlg.PostFk = PostFKs[0];
            dlg.PersonalName = string.Empty;
            dlg.PersonalFName = string.Empty;
            dlg.NumerPP = 0;
            dlg.NumerPU = 0;
            dlg.BirthData = DateTime.UtcNow;

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    

                    // Добавляем новую книгу в таблицу Books с использованием параметрического запроса
                    string sqlString = "INSERT INTO Staf (FirstName, LastName, PostFk, N_Recruitment, N_Dismissal, DateBirth) VALUES (@FirstName, @LastName, @PostFk, @N_Recruitment, @N_Dismissal, @DateBirth )";
             
                    using (SqlCommand command = new SqlCommand(sqlString, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@FirstName", dlg.PersonalFName ));
                        command.Parameters.Add(new SqlParameter("@LastName", dlg.PersonalName));
                        command.Parameters.Add(new SqlParameter("@PostFK", dlg.PostFk));
                        command.Parameters.Add(new SqlParameter("@N_Recruitment", dlg.NumerPP));
                        command.Parameters.Add(new SqlParameter("@N_Dismissal", dlg.NumerPU));
                        command.Parameters.Add(new SqlParameter("@DateBirth", dlg.BirthData));
                        command.ExecuteNonQuery();//возвращает количество затронутых строк.
                    }

                    // Получаем Id добавленной записи и добавляем его в коллекцию bookIDs
                    string sqlGetIdString = "SELECT @@IDENTITY";
                    using (SqlCommand commandId = new SqlCommand(sqlGetIdString, connection))
                    {
                        object obj = commandId.ExecuteScalar();
                        SIDs.Add(Convert.ToInt64(obj));
                    }

                    // Добавляем новую книгу в список книг на форме
                    ListViewItem item = listView.Items.Add(new ListViewItem());
                    item.Text = dlg.PostName;
                    item.SubItems.Add(dlg.PersonalName.Trim());
                    item.SubItems.Add(dlg.PersonalFName.Trim());
                    item.SubItems.Add(dlg.NumerPP.ToString());
                    item.SubItems.Add(dlg.NumerPU.ToString());
                    item.SubItems.Add(dlg.BirthData.ToString());


                    // Добавляем внешний ключ должности нового сотрудник в список внешних ключей сотрудников
                    PostFKs.Add(dlg.PostFk);
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            listView.Focus();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;

            var dlg = new Staf();

            ListView.SelectedListViewItemCollection selectedItems = listView.SelectedItems;
            ListView.SelectedIndexCollection selectedIndices = listView.SelectedIndices;

            dlg.PostFk = PostFKs[selectedIndices[0]];
            dlg.PersonalName = selectedItems[0].SubItems[1].Text;
            dlg.PersonalFName = selectedItems[0].SubItems[2].Text;
            dlg.NumerPP = int.Parse(selectedItems[0].SubItems[3].Text);
            dlg.NumerPU = int.Parse(selectedItems[0].SubItems[4].Text);
            dlg.BirthData = DateTime.Parse(selectedItems[0].SubItems[5].Text);

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    // Изменяем данные персонала  в таблице Staf с использованием параметрического запроса
                    string sqlString = "UPDATE Staf SET PostFk = @PostFk, FirstName = @FirstName, LastName= @LastName, N_Recruitment = @N_Recruitment  , N_Dismissal = @N_Dismissal , DateBirth = @DateBirth  WHERE Id = @Id";
                    using (SqlCommand command = new SqlCommand(sqlString, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@Id", SIDs[selectedIndices[0]]));
                        command.Parameters.Add(new SqlParameter("@PostFk", dlg.PostFk));
                        command.Parameters.Add(new SqlParameter("@FirstName", dlg.PersonalName));
                        command.Parameters.Add(new SqlParameter("@LastName", dlg.PersonalFName));
                        command.Parameters.Add(new SqlParameter("@N_Recruitment", dlg.NumerPP));
                        command.Parameters.Add(new SqlParameter("@N_Dismissal", dlg.NumerPU));
                        command.Parameters.Add(new SqlParameter("@DateBirth", dlg.BirthData));
                        command.ExecuteNonQuery();
                    }

                    // Обновляем список на форме !!!
                    selectedItems[0].Text = dlg.PostName;
                    selectedItems[0].SubItems[1].Text = dlg.PersonalName.Trim();
                    selectedItems[0].SubItems[2].Text = dlg.PersonalFName.Trim();
                    selectedItems[0].SubItems[3].Text = dlg.NumerPP.ToString();
                    selectedItems[0].SubItems[4].Text = dlg.NumerPU.ToString();
                    selectedItems[0].SubItems[5].Text = dlg.BirthData.ToString();


                    // Обновляем внешний ключ автора измененной книги в списке внешних ключей авторов
                    PostFKs[selectedIndices[0]] = dlg.PostFk;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            listView.Focus();
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            buttonEdit_Click(sender, e);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            // Получаем список индексов выделенных на удаление книг
            ListView.SelectedIndexCollection selectedIndices = listView.SelectedIndices;

            // Если есть что удалять и пользователь подтверждает желание удалить
            if (selectedIndices.Count == 0 ||
                MessageBox.Show("Вы действительно хотите удалить записи?", "Удаление записей", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            // Удаляем книги из списка снизу вверх
            for (int i = selectedIndices.Count - 1; i >= 0; i--)
            {
                try
                {
                    // Удаляем книгу при помощи параметрического запроса
                    string sqlString = "DELETE FROM Staf WHERE Id = @Id";
                    using (SqlCommand command = new SqlCommand(sqlString, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@Id", SIDs[selectedIndices[i]]));
                        command.ExecuteNonQuery();
                    }

                    // Удаляем Id и внешний ключ автора удаленной книги из коллекций bookIDs и authorFKs
                    SIDs.RemoveAt(selectedIndices[i]);
                    PostFKs.RemoveAt(selectedIndices[i]);
                    // Удаляем строку из списка на форме
                    listView.Items.RemoveAt(selectedIndices[i]);
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            listView.Focus();

        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
