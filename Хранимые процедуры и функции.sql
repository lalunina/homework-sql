--�������� ���������:
--1. �������� �������� ���������, ��������� �� ����� ������ ���������,
--   �� ������� �����.

-- ������� ���������� ������ ���������, ���� ����� ����������
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GoodStudents' AND type = 'P')
    DROP PROCEDURE GoodStudents;
GO

CREATE PROCEDURE GoodStudents
AS

SELECT S.FirstName, SC.DateIn 
        FROM StudentCards SC JOIN Students S ON S.Id= SC.StudentFk
        WHERE SC.DateIn IS NOT NULL
GO

-- �������� ��������� ���������
EXEC GoodStudents;
GO

-- �������� ��������� ���������
GoodStudents;
GO

--2. �������� �������� ���������, ������������ ��� � ������� ������������,
--   ��������� ���������� ���-�� ����.
--
---- ������� ���������� ������ ���������, ���� ����� ����������
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GoodStudents' AND type = 'P')
    DROP PROCEDURE GoodStudents;
GO

CREATE PROCEDURE GoodStudents
AS

SELECT top (1)  with TIES  L.FirstName, COUNT (SC.Id)
        FROM StudentCards SC JOIN Libs L ON L.Id= SC.LibFk

		GROUP BY L.FirstName ORDER BY  L.FirstName;
GO

-- �������� ��������� ���������
EXEC GoodStudents;
GO

--3. �������� �������� ���������, �������������� ��������� �����.
--
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Factorial' AND type = 'P')
    DROP PROCEDURE Factorial;
GO

CREATE PROCEDURE Factorial
AS
Declare @X int, @Y int

Set NoCount On

Set @X=1
Set @Y=1

While (@Y<=5)
begin
 Select @X=@X*@Y
 Select @Y=@Y+1
end

Set NoCount OFF

Select Result=@X

GO
-- �������� ��������� ���������
EXEC Factorial;
GO

--�������:
--1. �������, ������������ ���-�� ���������, ������� �� ����� �����.
--
---- ������� ���������� ������ inline-�������, ���� ����� ����������
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'StupidStudents' AND type = 'IF')
    DROP FUNCTION StupidStudents;
GO

-- ������� ������� 
CREATE FUNCTION StupidStudents()
RETURNS TABLE
AS
RETURN
   (SELECT S.FirstName AS StName
    FROM  Students S LEFT JOIN StudentCards SC ON  S.Id !=SC.StudentFk
	);
GO

-- ���������� ������� ��� �������� ������ ��� �������
SELECT COUNT(StName) AS ��������_�������_��_�����_���� FROM StupidStudents();
GO

--2. �������, ������������ ����������� �� ���� ���������� ����������.
--
-- ������� ���������� ������ scalar-�������, ���� ����� ����������
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GetMin' AND type = 'FN')
    DROP FUNCTION GetMin;
GO

-- ������� �������
CREATE FUNCTION GetMin(@a int, @b int, @c int)
RETURNS int
AS
BEGIN

DECLARE @massiv TABLE  (i int)
DECLARE @min int

	BEGIN
	INSERT @massiv (i) VALUES (@a)
	INSERT @massiv (i) VALUES (@b)
	INSERT @massiv (i) VALUES (@c)
	
	END;
	SELECT  @min = min(i)
	From @massiv
	RETURN @min;
END;
GO

-- ������� ����� �������
DECLARE @min int;
EXEC @min = GetMin 2, 5,7;
SELECT @min;
GO

-- ������ ������� ������ �������
SELECT dbo.GetMin(2, 5, 1);
GO
--
--3. �������, ������� ��������� � �������� ��������� ������������� �����
--   � ���������� ����� �� �������� ������, ���� ��� �����.
--   (����������� % - ������� � �������. ��������: 57 % 10 = 7.)
-- ������� ���������� ������ scalar-�������, ���� ����� ����������
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GetRaz' AND type = 'IF')
    DROP FUNCTION GetRaz;
GO

-- ������� �������
CREATE FUNCTION GetRaz (@a int )
RETURNS nvarchar(50)
AS
BEGIN

   DECLARE @n1 int = @a%10;
   DECLARE @n2 int = @a/10;
   DECLARE @res nvarchar (50);

	BEGIN
	IF (@n1 > @n2)
	SET @res = '������ ������ ������';
	ELSE IF (@n1 < @n2)
	SET @res = '������ ������ ������';
	ELSE IF (@n1 = @n2)
	SET @res = '������� �����';
	
	END;
	RETURN @res;
END;
GO

-- ������� ����� �������
DECLARE @res nvarchar(50);
EXEC @res = GetRaz 34;
SELECT @res;
GO

-- ������ ������� ������ �������
SELECT dbo.GetRaz(82);
GO



--4. �������, ������������ ���-�� ������ ���� �� ������ �� ����� �
--   �� ������ �� ������ (departments).

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'GropByGroupAndFacult' )
    DROP FUNCTION GropByGroupAndFacult;
GO

-- ������� ������� 
CREATE FUNCTION GropByGroupAndFacult()
RETURNS TABLE
AS
RETURN
   ( 
 SELECT G.Name AS Name, SC.BookFk AS Book
 FROM  (StudentCards SC JOIN Students S ON S.Id=SC.StudentFk) JOIN Groups G ON G.Id = S.GroupFk
 --GROUP BY G.Name
 UNION 
SELECT D.Name AS Name,  TC.BookFk AS Book
 FROM  (TeacherCards TC JOIN Teachers T ON T.Id=TC.TeacherFk) JOIN Departments D ON D.Id = T.DepartmentFk
 --GROUP BY D.Name
	
	);
GO

-- ���������� ������� ��� �������� ������ ��� �������
SELECT  Name, COUNT(Book) AS  Book  FROM GropByGroupAndFacult() 
GROUP BY Name;
GO
 

--5. �������, ������������ ������ ����, ���������� ������ ���������
--   (��������, ��� ������, ������� ������, ��������, ���������),
--   � ��������������� �� ������ ����, ���������� � 5-� ���������,
--   � �����������, ��������� � 6-� ���������.
--

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InfoOfBooks' )
    DROP FUNCTION InfoOfBooks;
GO

-- ������� ������� 
CREATE FUNCTION InfoOfBooks( @AtuthorsName nvarchar(50), @AutorsFName nvarchar(50), @Thems nvarchar(50), @Categories nvarchar(50))--, @orderBy int, @direction nvarchar(50) )

RETURNS TABLE
AS
   
RETURN
    (SELECT B.Name AS Book_Name, A.FirstName AS A_FName, A.LastName  AS A_LName, T.Name AS Thems, C.Name AS Categories
    FROM  ((Books B JOIN  Authors A ON A.Id =B.AuthorFk) JOIN Themes T ON T.Id =B.ThemeFk)  JOIN Categories C ON C.Id=B.CategoryFk 
	WHERE A.FirstName = @AtuthorsName AND A.LastName = @AutorsFName AND T.Name = @Thems AND C.Name = @Categories

	);
GO

-- ���������� ������� ��� �������� ������ ��� �������
SELECT Book_Name  FROM InfoOfBooks('�������', '�������������','����������������','C++ Builder')
;
GO
 



--
--6. �������, ������� ���������� ������ ������������� � ���-�� ��������
--   ������ �� ��� ����.

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'LibsAndBooks' )
    DROP FUNCTION LibsAndBooks;
GO

-- ������� ������� 
CREATE FUNCTION LibsAndBooks()
RETURNS TABLE
AS
RETURN
   ( 
 SELECT L.FirstName AS Name, SC.BookFk AS Book
 FROM  StudentCards SC JOIN Libs L ON L.Id = SC.LibFk
 UNION 
 SELECT L.FirstName AS Name,  TC.BookFk AS Book
 FROM  TeacherCards TC JOIN Libs L ON L.Id=TC.LibFk	
	);
GO

-- ���������� ������� ��� �������� ������ ��� �������
SELECT  Name, COUNT(Book) AS  Book  FROM LibsAndBooks() 
GROUP BY Name;
GO
 
 -- 5 �������
--
-- IF EXISTS (SELECT * FROM sys.objects WHERE name = 'InfoOfBooks' )
--    DROP FUNCTION InfoOfBooks;
--GO
--
---- ������� ������� 
--CREATE FUNCTION InfoOfBooks( @AtuthorsName nvarchar(50), @AutorsFName nvarchar(50), @Thems nvarchar(50), @Categories nvarchar(50), @orderBy int, @direction nvarchar(50) )
--
--RETURNS TABLE  (ID int  PRIMARY KEY , Book_Name nvarchar(50),  AtuthorsName nvarchar(50), AutorsFName nvarchar(50), Thems nvarchar(50), Categories nvarchar(50))
--AS
--   BEGIN
--   -- ��������� ���������� ���� "�������" (�� ���� � ��������� �������)
-- DECLARE @BookInfo TABLE  (ID int DEFAULT 0, Book_Name nvarchar(50), AtuthorsName nvarchar(50), AutorsFName nvarchar(50), Thems nvarchar(50), Categories nvarchar(50));
-- DECLARE @ord int = @orderBy;
-- DECLARE @direct nvarchar(50) =@direction;
--	
--	INSERT  INTO  @BookInfo 
--	   SELECT B.Name AS Book_Name, A.FirstName AS A_FName, A.LastName  AS A_LName, T.Name AS Thems, C.Name AS Categories
--       FROM  ((Books B JOIN  Authors A ON A.Id =B.AuthorFk) JOIN Themes T ON T.Id =B.ThemeFk)  JOIN Categories C ON C.Id=B.CategoryFk	
--	   WHERE A_FName = @AtuthorsName AND A.LastName = @AtuthorsName AND T.Name = @Thems AND C.Name = @Categories)
--	
--
-- 
--
--RETURN
-- END;  
--GO
--
---- ���������� ������� ��� �������� ������ ��� �������
--SELECT Book_Name  FROM InfoOfBooks('�������', '�������������','����������������','C++ Builder')
--;
--GO
 