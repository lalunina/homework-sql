-- ������� ���� ������

IF DB_ID('BookShop') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE BookShop SET single_user with rollback immediate
    DROP DATABASE  BookShop 
END
GO

CREATE DATABASE BookShop
GO

USE BookShop
GO


-- ������� �������


CREATE TABLE �ountries
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	
	�OD char(15)
)
GO

CREATE TABLE �ities
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	�ountryFK bigint,
	FOREIGN KEY (�ountryFK) REFERENCES �ountries(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE
)
GO

CREATE TABLE Departments--�����
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL, -- �������� ������
)
GO

CREATE TABLE Shops
(
Number bigint NOT NULL PRIMARY KEY,
Address nvarchar(100),
�ountryFK bigint,

FOREIGN KEY (�ountryFK) REFERENCES �ountries(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,
)
GO

CREATE TABLE  Staf--����
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	DepartmentFK bigint,
	ShopFK bigint,
	
    
	

	FOREIGN KEY (DepartmentFK) REFERENCES Departments(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,
	FOREIGN KEY (ShopFK ) REFERENCES Shops(Number )
	    ON DELETE CASCADE
        ON UPDATE CASCADE
)
GO



CREATE TABLE  Buyers-- ����������
(
   Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	Address nvarchar(100),
	�ity bigint,
  
  FOREIGN KEY (�ity) REFERENCES �ities(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE
	
)
GO

CREATE TABLE Presses-- ������������
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	Address nvarchar(100),
	�ountryFK bigint,
	Phone char(15),
	
	FOREIGN KEY (�ountryFK) REFERENCES �ountries(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE
)
GO

CREATE TABLE Genres --����
(
    Id bigint NOT NULL PRIMARY KEY,--��������� ��� ��� ��������� ���� ��������������� ��� ����� ����
	Name nvarchar(50) NOT NULL,
	DepartmentFK bigint,

		FOREIGN KEY (DepartmentFK) REFERENCES Departments(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,
)
GO



CREATE TABLE Authors
(
    Id bigint NOT NULL PRIMARY KEY,
	Name nvarchar(50) NOT NULL,--���
	BirthYear int DEFAULT 1900 CHECK(BirthYear > -5000),
	�ountryFK bigint,


	FOREIGN KEY (�ountryFK) REFERENCES �ountries(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE
		

)
GO

CREATE TABLE  Invoices-- ����
(
   Number bigint NOT NULL PRIMARY KEY,
   PersonalFK bigint,
   BuyerFK bigint,

	FOREIGN KEY (PersonalFK) REFERENCES Staf(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,


	FOREIGN KEY (BuyerFK ) REFERENCES Buyers(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,
)--����
GO

CREATE TABLE Books
(
    Id bigint  NOT NULL,
	Name nvarchar(50) NOT NULL,
	AuthorFk bigint,
	PressFk bigint,
	Pages int,
	Price money,-- ���������
	Genre bigint,

	PRIMARY KEY (Id),

	FOREIGN KEY (AuthorFk) REFERENCES Authors(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,
	FOREIGN KEY (PressFk) REFERENCES Presses(Id)
	    ON DELETE NO ACTION
        ON UPDATE NO ACTION,

	FOREIGN KEY (Genre) REFERENCES Genres(Id)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,
	CONSTRAINT PagesConstr CHECK(Pages > 0),
)
GO

CREATE TABLE Sales
(
Id bigint NOT NULL PRIMARY KEY,
BookFK bigint,
InvoiceFK bigint,
qt int,--���-��

FOREIGN KEY (BookFK) REFERENCES Books(ID)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,

FOREIGN KEY (InvoiceFK) REFERENCES Invoices(Number)
	    ON DELETE CASCADE
        ON UPDATE CASCADE,


)
GO

-- ��������� �������

INSERT INTO �ountries VALUES (1, '�������',804)
INSERT INTO �ountries VALUES (2, '������', 643)
INSERT INTO �ountries VALUES (3, '������',616)
INSERT INTO �ountries VALUES (4, '��������������',660)

INSERT INTO �ities VALUES (1, '����', 1)
INSERT INTO �ities VALUES (2, '������', 1)
INSERT INTO �ities VALUES (3, '������', 1)
INSERT INTO �ities VALUES (4, '�������', 1)

INSERT INTO Departments VALUES (1,'����� ������� ����������')
INSERT INTO Departments VALUES (2,'�����  ����������')
INSERT INTO Departments VALUES (3,'�����  �������� ����������')

INSERT INTO Shops  VALUES (1,  '��. ����������', 1)
INSERT INTO Shops  VALUES (2, '��. ����������', 2)
INSERT INTO Shops  VALUES (3, '��. ����������', 1)

INSERT INTO Staf  VALUES (1, '�������', 1, 1)
INSERT INTO Staf  VALUES (2, '�����', 2, 3)
INSERT INTO Staf  VALUES (3, '�������', 3, 2)
INSERT INTO Staf  VALUES (4, '�����', 2, 3)


INSERT INTO Buyers  VALUES (1, '����', '��. ��������' , 1)
INSERT INTO Buyers  VALUES (2, 'C����', '��. �������� ', 2)

INSERT INTO Genres  VALUES (1,' ����������',2)
INSERT INTO Genres  VALUES (2, '������',1)
INSERT INTO Genres  VALUES (3, '������ 20-�� ����',3)
INSERT INTO Genres  VALUES (4, '�������',3)

INSERT INTO Authors VALUES (1, '������ ����������', 1959,1)
INSERT INTO Authors VALUES (2, '������ ��������', 1900,1)
INSERT INTO Authors VALUES (3, '������� ���� �������', 1900,4)

INSERT INTO Presses (Id, Name, Phone) VALUES (1, '�����', '+3801234567890') --���� ���������� �����-�� ����, �� � ������� ��������� ����� ����� ����� ���������INSERT INTO Presses (Id, Name, Phone) VALUES (2, '����-����-��������� ������', '+3801234567891')
INSERT INTO Presses (Id, Name, Phone) VALUES (2, '�������', '+3801234567892')

INSERT INTO Books VALUES (1, '������ ���� �����', 1, 1, 300, 150,1 )
INSERT INTO Books VALUES (2, '��� � �����������', 1, 1, 400, 170,1)
INSERT INTO Books VALUES (3, '����� ����� �����', 1, 2, 250, 135,1)
INSERT INTO Books VALUES (4, '������ � ���������', 2, 2, 500, 250,3)
INSERT INTO Books VALUES (5, '������. ���� � �������', 3, 2, 150, 150,4)
INSERT INTO Books VALUES (6, '��������� �����', 3, 2, 1500, 500,1)



INSERT INTO Invoices VALUES (1,1,1 )
INSERT INTO Invoices VALUES (2,1,2)
INSERT INTO Invoices VALUES (3,2,2 )
INSERT INTO Invoices VALUES (4,2,1 )


INSERT INTO Sales  VALUES (1,1,1,1)
INSERT INTO Sales  VALUES (2,2,2,1)
INSERT INTO Sales  VALUES (3,2,3,1)
INSERT INTO Sales  VALUES (4,3,2,1)

GO
-- ���������

SELECT A.Name AS Author, B.Name, P.Name AS Presses
FROM Authors A, Presses P, Books B -- ������ �� ������ ������
WHERE A.Id = B.AuthorFk AND P.Id = B.PressFk -- ������� ������ �������� �� ������

SELECT��Name

FROM Books

WHERE��

(SELECT ID

FROM Presses

WHERE Name = '�����')� =� �PressFk

--8. ����� �������, ������� ����� � ��� �������, � ������� ���� ������ ���� ������� �� ������ ����, ��������� � ��. 
--��������� ������� ��������� � ��������� �������������.


CREATE VIEW C_A_AND_S�
AS
SELECT A.Name, A.�ountryFK AS ������, S.Number AS �����
FROM (Authors A JOIN  Books B ON A.Id = B.AuthorFk)
                  JOIN Shops S ON  A.�ountryFK =  S.�ountryFK;
				 


SELECT  CS.Name AS �����, C.Name AS ������
FROM  �ountries C JOIN C_A_AND_S� CS ON C.Id = CS.������
GROUP BY   CS.Name, C.Name  order BY C.Name desc;

--9. �������� �������������,������� �������� ������ ������� ����� ��������, �������� WEB -��������������

CREATE VIEW PriseBooks
AS
SELECT B.Name, B.Price AS BookPrice
FROM Books B JOIN  Genres G ON B.Genre = G.ID
WHERE G.Name = ' ����������' ;

SELECT  B.Name, B.Price
FROM Books B
WHERE B.Price =
     (SELECT MAX (P.BookPrice) FROM PriseBooks P);



--10. �������� �������������, ������� ��������� ������ ��� ���������� � ������ ���������. 
    --������������� ������� �� ������� �� ����������� � �� �������� ���������.

CREATE VIEW ShopInfo
AS
SELECT S.Address AS  �����, C.Name AS  ������, S.Number AS �����_��������
FROM Shops S  JOIN   �ountries C   ON C.Id = S.�ountryFK;

--Drop VIEW  ShopInfo;

SELECT *
From ShopInfo
ORDER BY ������, �����_��������;


--11. �������� ������������� �������������, ������� ���������� ����� ���������� �����.

CREATE VIEW POPBook
AS
SELECT I.Number  AS ���������, B.Name AS �����
FROM (Books B JOIN Sales S ON S.BookFK = B.Id)
     JOIN  Invoices I ON S.InvoiceFK = I.Number;

SELECT top (1)  with TIES P.�����, COUNT (P.�����) AS �������_���
FROM POPBook P
GROUP BY P.����� order BY �������_��� desc;

DROP VIEW POPBook;

--12. �������� ���������������� �������������, � ������� ������ ���������� ���  �������, ����� ������� ���������� � � ��� �.
CREATE VIEW A_Book
AS
SELECT A.Name
FROM Authors A
WHERE A.Name LIKE '�%' OR A.Name LIKE '�%' ;

SELECT *
FROM A_Book;

--DROP VIEW A_Book;


13--�������� �������������, � ������� � ������� ����������� ������� �������� ���������, ������� �� ������� ����� ������ ������������.

CREATE VIEW Book_Presses
AS
SELECT  SH.Number AS �����_�������� ,  B.Name AS �����, P.Name AS �����������
FROM ((((Shops SH JOIN Staf S  ON S.ShopFK = SH.Number)
             JOIN Invoices I ON I.PersonalFK = S.Id)
			 JOIN Sales SL ON SL.InvoiceFK = I.Number)
			 JOIN Books B ON B.Id = SL.BookFK)
			 JOIN Presses P  ON P.Id =B.PressFk;

SELECT *
FROM Book_Presses
WRERE ;
             