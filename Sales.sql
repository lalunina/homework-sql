
-- ������� ���� ������

IF DB_ID('Accounting') IS NOT NULL
BEGIN
	USE master
    ALTER DATABASE Accounting SET single_user with rollback immediate;
    DROP DATABASE Accounting;
END
GO

CREATE DATABASE Accounting;
GO


-- ������ ��������� ���� ������ �������

USE Accounting
GO


-- ������� �������

-- 
-- Book Staff
-- 

CREATE TABLE Buyers
(
	Id int NOT NULL PRIMARY KEY,
	FirstName varchar(50),
	LastName varchar(50),
)
GO

CREATE TABLE Sellers
(
	Id int NOT NULL PRIMARY KEY,
	FirstName varchar(50),
	LastName varchar(50),
)
GO

 --Sales ���������� ����������

 CREATE TABLE Sales
(
	Id int NOT NULL PRIMARY KEY,
	BuyerFK bigint,
	SellerFK bigint,
	Summa bigint,
	Date date,
)
GO


--team positions - ������� � �������

INSERT Buyers (Id, FirstName, LastName) VALUES (1, N'�������', N'��������')
INSERT Buyers (Id, FirstName, LastName) VALUES (2,  N'����', N'������')
INSERT Buyers (Id, FirstName, LastName) VALUES (3,  N'������', N'��������')
INSERT Buyers (Id, FirstName, LastName) VALUES (4,  N'��������', N'������')
INSERT Buyers (Id, FirstName, LastName) VALUES (5,  N'�����', N'�������')
INSERT Buyers (Id, FirstName, LastName) VALUES (6,  N'�������', N'����')
INSERT Buyers (Id, FirstName, LastName) VALUES (7,  N'�����', N'����')
INSERT Buyers (Id, FirstName, LastName) VALUES (5,  N'������', N'���')
				   
INSERT Sellers(Id, FirstName, LastName) VALUES (1, N'�����', N'�����')
INSERT Sellers(Id, FirstName, LastName) VALUES (2, N'����', N'����')
INSERT Sellers(Id, FirstName, LastName) VALUES (3, N'������', N'��������')

INSERT  Sales (Id, BuyerFK, SellerFK, Summa, Date) VALUES (1, 1, 1,500, '2020.02.12' )
INSERT  Sales (Id, BuyerFK, SellerFK, Summa, Date) VALUES (2,2, 1,800, '2020.02.13')
INSERT  Sales (Id, BuyerFK, SellerFK, Summa, Date) VALUES (3,4, 1,200, '2020.02.13')
INSERT  Sales (Id, BuyerFK, SellerFK, Summa, Date) VALUES (4,5, 2,300, '2020.02.14')
INSERT  Sales (Id, BuyerFK, SellerFK, Summa, Date) VALUES (5,6, 2,400, '2020.02.14')
INSERT  Sales (Id, BuyerFK, SellerFK, Summa, Date) VALUES (6,7, 3,700, '2020.02.15')
