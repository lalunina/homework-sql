﻿using Microsoft.Win32;
using System;
using System.Diagnostics;

namespace DataTime
{
    class Program
    {
        

        static void Main(string[] args)
        {

            Console.WriteLine(DateTime.Now);

            Console.WriteLine($"My EXE-file Full Path: {AppDomain.CurrentDomain.BaseDirectory}{AppDomain.CurrentDomain.FriendlyName}.exe");
            try
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true))
                {
                    if (key != null)
                    {
                        key.SetValue("MyKey", $"{AppDomain.CurrentDomain.BaseDirectory}{AppDomain.CurrentDomain.FriendlyName}.exe");
                        key.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message );
            }
        }
    }
}
